package ru.skillbranch.catalogapp.di.modules;

import android.content.Context;

import com.facebook.stetho.Stetho;
import com.uphyca.stetho_realm.RealmInspectorModulesProvider;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import ru.skillbranch.catalogapp.data.storage.RealmManager;

@Module
class FlavorLocalModule {

    private static final String TAG = "BASE";

    @Provides
    @Singleton
    RealmManager provideRealmManager (Context context) {
        Stetho.initialize(Stetho.newInitializerBuilder(context)
                .enableDumpapp(Stetho.defaultDumperPluginsProvider(context))
                .enableWebKitInspector(RealmInspectorModulesProvider.builder(context).build())
                .build()
        );
        return new RealmManager();
    }
}