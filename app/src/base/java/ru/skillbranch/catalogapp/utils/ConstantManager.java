package ru.skillbranch.catalogapp.utils;

import ru.skillbranch.catalogapp.BuildConfig;

public interface ConstantManager {

    String TAG_PREFIX = "MVP ";

    String FONT_BOOK = "PTBebasNeueBook";
    String FONT_REGULAR = "PTBebasNeueRegular";
    String FONT_COMIC = "comic";

    //String PATTERN_PASSWORD = "^\\\\S{8,}$";
    String PATTERN_EMAIL = "\\w+@\\w+.\\w{2,}";
    String PATTERN_PHONE = "^((8|\\+7)[\\- ]?)?(\\(?\\d{3}\\)?[\\- ]?)?[\\d\\- ]{7,10}$";

    int REQUEST_PERMISSION_CAMERA = 300;
    int REQUEST_PERMISSION_READ_EXTERNAL_STORAGE = 301;

    int REQUEST_PROFILE_PHOTO_CAMERA = 100;
    int REQUEST_PROFILE_PHOTO_PICKER = 101;

    String FILE_PROVODER_AUTHORITY = BuildConfig.APPLICATION_ID + ".fileprovider";
    String FILE_DONT_CREATE_MESSAGE = "Фотография не может быть создана";

    String LAST_MODIFIED_HEADER = "Last-Modified";
    String IF_MODIFIED_SINCE_HEADER = "If-Modified-Since";

    String PRODUCT_COST_SUFFIX = ".-";
}