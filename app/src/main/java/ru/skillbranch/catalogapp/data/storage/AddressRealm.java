package ru.skillbranch.catalogapp.data.storage;

import java.util.UUID;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import ru.skillbranch.catalogapp.data.dto.AddressDto;

public class AddressRealm extends RealmObject {
    @PrimaryKey
    private String id;
    private String name;
    private String street;
    private String house;
    private String apartment;
    private String floor;
    private String comment;
    private boolean favorite;

    public AddressRealm() {
    }

    public AddressRealm(AddressDto addressDto) {
        this.id = addressDto.getId();
        if (this.id.isEmpty()) {
            this.id = UUID.randomUUID().toString();
        }
        this.name = addressDto.getName();
        this.street = addressDto.getStreet();
        this.house = addressDto.getHouse();
        this.apartment = addressDto.getApartment();
        this.floor = addressDto.getFloor();
        this.comment = addressDto.getComment();
        this.favorite = addressDto.isFavorite();
    }

    // region ==================== Getters ====================

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getStreet() {
        return street;
    }

    public String getHouse() {
        return house;
    }

    public String getApartment() {
        return apartment;
    }

    public String getFloor() {
        return floor;
    }

    public String getComment() {
        return comment;
    }

    public boolean isFavorite() {
        return favorite;
    }

    // endregion
}