package ru.skillbranch.catalogapp.data.network.error;

public class ApiError extends Throwable {
    private int statusCode;
    private String message;

    @Override
    public String getMessage() {
        return message;
    }
}
