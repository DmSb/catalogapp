package ru.skillbranch.catalogapp.data.dto;

import android.support.annotation.NonNull;

public class PermissionResultDto {
    private int requestCode;
    @NonNull
    private String[] permissions;
    @NonNull
    private int[] grantResults;

    public PermissionResultDto(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        this.requestCode = requestCode;
        this.permissions = permissions;
        this.grantResults = grantResults;
    }

    public int getRequestCode() {
        return requestCode;
    }

    @NonNull
    public String[] getPermissions() {
        return permissions;
    }

    @NonNull
    public int[] getGrantResults() {
        return grantResults;
    }
}
