package ru.skillbranch.catalogapp.data.storage;

import java.util.Date;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import ru.skillbranch.catalogapp.data.dto.CommentDto;
import ru.skillbranch.catalogapp.data.network.res.CommentRes;

public class CommentRealm extends RealmObject {
    @PrimaryKey
    private String id;
    private String userName;
    private String avatar;
    private float rating;
    private Date commentDate;
    private String comment;

    public CommentRealm() {
    }

    public CommentRealm(String userName, String avatar, float rating, String comment) {
        this.id = String.valueOf(this.hashCode());
        this.userName = userName;
        this.rating = rating;
        this.comment = comment;
        this.commentDate = new Date();
        this.avatar = avatar;
    }

    CommentRealm(CommentRes commentRes) {
        this.id = commentRes.getId();
        this.userName = commentRes.getUserName();
        this.avatar = commentRes.getAvatar();
        this.rating = commentRes.getRating();
        this.commentDate = commentRes.getCommentDate();
        this.comment = commentRes.getComment();
    }

    public CommentRealm(CommentDto commentDto) {
        this.id = commentDto.getId();
        this.userName = commentDto.getUserName();
        this.avatar = commentDto.getAvatar();
        this.rating = commentDto.getRating();
        this.commentDate = commentDto.getCommentDate();
        this.comment = commentDto.getComment();
    }

    // region ==================== Getters ====================

    public String getId() {
        return id;
    }

    public String getUserName() {
        return userName;
    }

    public String getAvatar() {
        return avatar;
    }

    public float getRating() {
        return rating;
    }

    public Date getCommentDate() {
        return commentDate;
    }

    public String getComment() {
        return comment;
    }

    // endregion
}