package ru.skillbranch.catalogapp.data.network.res;

import com.squareup.moshi.Json;

class CommentJson {
    @Json(name = "_id")
    private String id;
    private String avatar;
    private String userName;
    @Json(name = "raiting")
    private float rating;
    private String commentDate;
    private String comment;
    private boolean active;

    // region ==================== Getters ====================

    public String getId() {
        return id;
    }

    public String getAvatar() {
        return avatar;
    }

    public String getUserName() {
        return userName;
    }

    public float getRating() {
        return rating;
    }

    String getCommentDate() {
        return commentDate;
    }

    public String getComment() {
        return comment;
    }

    boolean isActive() {
        return active;
    }

    // endregion

    // region ==================== Setters ====================

    public void setId(String id) {
        this.id = id;
    }

    void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public void setRating(float rating) {
        this.rating = rating;
    }

    void setCommentDate(String commentDate) {
        this.commentDate = commentDate;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    void setActive(boolean active) {
        this.active = active;
    }


    // endregion
}
