package ru.skillbranch.catalogapp.data.dto;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.Nullable;

import com.squareup.moshi.JsonAdapter;
import com.squareup.moshi.Moshi;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import ru.skillbranch.catalogapp.data.network.res.CommentRes;
import ru.skillbranch.catalogapp.data.network.res.ProductRes;
import ru.skillbranch.catalogapp.data.storage.ProductRealm;
import ru.skillbranch.catalogapp.utils.ConstantManager;

public class ProductDto implements Parcelable {
    private String id;
    private String productName;
    private String imageUrl;
    private String description;
    private int price;
    private int count = 0;
    private boolean favorite = false;
    private float rating;
    private List<CommentDto> comments = null;

    public ProductDto(String id, String productName, String imageUrl, String description,
                      int price, int count, boolean favorite, float rating, List<CommentDto> comments) {
        this.id = id;
        this.productName = productName;
        this.imageUrl = imageUrl;
        this.description = description;
        this.price = price;
        this.count = count;
        this.favorite = favorite;
        this.rating = rating;
        this.comments = comments;
    }

    public ProductDto(ProductRes productRes, ProductLocalDto productLocalDto) {
        this.id = productRes.getId();
        this.productName = productRes.getProductName();
        this.imageUrl = productRes.getImageUrl();
        this.description = productRes.getDescription();
        this.price = productRes.getPrice();
        this.rating = productRes.getRating();

        if (productLocalDto != null) {
            this.count = productLocalDto.getCount();
            this.favorite = productLocalDto.isFavorite();
        }

        this.comments = new ArrayList<>();
        if (productRes.getComments() != null) {
            for (CommentRes commentRes : productRes.getComments()) {
                this.comments.add(new CommentDto(
                        commentRes.getId(),
                        commentRes.getUserName(),
                        commentRes.getAvatar(),
                        commentRes.getRating(),
                        commentRes.getCommentDate(),
                        commentRes.getComment()
                ));
            }
        }
    }

    public ProductDto(ProductRealm product) {
        this.id = product.getId();
        this.productName = product.getProductName();
        this.imageUrl = product.getImageUrl();
        this.description = product.getDescription();
        this.price = product.getPrice();
        this.count = product.getCount();
        this.favorite = product.isFavorite();
        this.rating = product.getRating();
    }

    /*
    private ProductDto(int id, String productName, String imageUrl, String description,
                       int price, float rating, List<CommentDto> comments) {
        this.id = id;
        this.productName = productName;
        this.imageUrl = imageUrl;
        this.description = description;
        this.price = price;
        this.count = 0;
        this.favorite = false;
        this.rating = rating;
        this.comments = comments;
    }*/

    // region ==================== Json  ====================

    @Nullable
    public static ProductDto fromJson(String jsonValue) {
        Moshi moshi = new Moshi.Builder().build();
        JsonAdapter<ProductDto> jsonAdapter = moshi.adapter(ProductDto.class);
        try {
            return jsonAdapter.fromJson(jsonValue);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public String toJson() {
        Moshi moshi = new Moshi.Builder().build();
        JsonAdapter<ProductDto> jsonAdapter = moshi.adapter(ProductDto.class);
        return jsonAdapter.toJson(this);
    }

    // endregion

    // region ============================ Getters =================================================

    public String getId() {
        return id;
    }

    public String getProductName() {
        return productName;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public String getDescription() {
        return description;
    }

    public int getPrice() {
        return price;
    }

    public int getCount() {
        return count;
    }

    public boolean isFavorite() {
        return favorite;
    }

    public float getRating() {
        return rating;
    }

    public String getFormatPrice() {
        return Integer.toString(price) + ConstantManager.PRODUCT_COST_SUFFIX;
    }

    public List<CommentDto> getComments() {
        return comments;
    }

    // endregion

    // region ==================== Setters ====================

    public void setPrice(int price) {
        this.price = price;
    }


    // endregion

    // region ================================= Parcelable =========================================

    public ProductDto(Parcel in) {
        this.id = in.readString();
        this.productName = in.readString();
        this.imageUrl = in.readString();
        this.description = in.readString();
        this.price = in.readInt();
        this.count = in.readInt();
    }

    public static final Creator<ProductDto> CREATOR = new Creator<ProductDto>() {
        @Override
        public ProductDto createFromParcel(Parcel in) {
            return new ProductDto(in);
        }

        @Override
        public ProductDto[] newArray(int size) {
            return new ProductDto[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(id);
        parcel.writeString(productName);
        parcel.writeString(imageUrl);
        parcel.writeString(description);
        parcel.writeInt(price);
        parcel.writeInt(count);
    }

    public void deleteProduct() {
        count--;
    }

    public void addProduct() {
        count++;
    }

    // endregion
}