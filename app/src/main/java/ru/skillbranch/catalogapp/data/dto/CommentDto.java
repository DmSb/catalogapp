package ru.skillbranch.catalogapp.data.dto;

import java.util.Date;

import ru.skillbranch.catalogapp.data.storage.CommentRealm;

public class CommentDto {

    private String id;
    private String userName;
    private String avatar;
    private float rating;
    private Date commentDate;
    private String comment;

    CommentDto(String id, String userName, String avatar, float rating, Date commentDate, String comment) {
        this.id = id;
        this.userName = userName;
        this.avatar = avatar;
        this.rating = rating;
        this.commentDate = commentDate;
        this.comment = comment;
    }

    public CommentDto(CommentRealm comment) {
        this.id = comment.getId();
        this.userName = comment.getUserName();
        this.avatar = comment.getAvatar();
        this.rating = comment.getRating();
        this.commentDate = comment.getCommentDate();
        this.comment = comment.getComment();
    }

    public String getId() {
        return id;
    }

    public String getUserName() {
        return userName;
    }

    public String getAvatar() {
        return avatar;
    }

    public float getRating() {
        return rating;
    }

    public String getComment() {
        return comment;
    }

    public String getAge() {
        Date nowDate = new Date();
        long ago = nowDate.getTime();

        ago = ago - commentDate.getTime();
        long agoMinute = ago / 1000 / 60;
        long agoHour = agoMinute / 60;
        long agoDay = agoHour / 24;
        long agoMonth = agoDay / 30;
        long agoYear = agoDay / 365;

        if (agoYear > 0) {
            return Long.toString(agoYear) + " years ago";
        } else if (agoMonth > 0) {
            return Long.toString(agoMonth) + " months ago";
        } else if (agoDay > 0) {
            return Long.toString(agoDay) + " days ago";
        } else if (agoHour > 0 ) {
            return Long.toString(agoHour) + " hours ago";
        } else if (agoMinute > 0) {
            return Long.toString(agoMinute) + " minutes ago";
        }
        return "";
    }

    public Date getCommentDate() {
        return commentDate;
    }

    public void setRatingInt(float rating) {
        this.rating = rating;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}