package ru.skillbranch.catalogapp.data.dto;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.Nullable;

import com.squareup.moshi.JsonAdapter;
import com.squareup.moshi.Moshi;

import java.io.IOException;

import ru.skillbranch.catalogapp.data.storage.AddressRealm;

public class AddressDto implements Parcelable {
    private String id;
    private String name;
    private String street;
    private String house;
    private String apartment;
    private String floor;
    private String comment;
    private boolean favorite;

    public AddressDto() {
        this.id = "";
        this.name = "";
        this.street = "";
        this.house = "";
        this.apartment = "";
        this.floor = "";
        this.comment = "";
        this.favorite = false;
    }

    public AddressDto(String id, String name, String street, String house, String apartment,
                      String floor, String comment, boolean favorite) {
        this.id = id;
        this.name = name;
        this.street = street;
        this.house = house;
        this.apartment = apartment;
        this.floor = floor;
        this.comment = comment;
        this.favorite = favorite;
    }

    public AddressDto(AddressRealm addressRealm) {
        this.id = addressRealm.getId();
        this.name = addressRealm.getName();
        this.street = addressRealm.getStreet();
        this.house = addressRealm.getHouse();
        this.apartment = addressRealm.getApartment();
        this.floor = addressRealm.getFloor();
        this.comment = addressRealm.getComment();
        this.favorite = addressRealm.isFavorite();
    }

    // region ==================== Json convert ====================

    public String toJson() {
        Moshi moshi = new Moshi.Builder().build();
        JsonAdapter<AddressDto> jsonAdapter = moshi.adapter(AddressDto.class);
        return jsonAdapter.toJson(this);
    }

    @Nullable
    public static AddressDto fromJson(String jsonValue) {
        Moshi moshi = new Moshi.Builder().build();
        JsonAdapter<AddressDto> jsonAdapter = moshi.adapter(AddressDto.class);
        try {
            return jsonAdapter.fromJson(jsonValue);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    // endregion

    // region ==================== Getters ====================

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getStreet() {
        return street;
    }

    public String getHouse() {
        return house;
    }

    public String getApartment() {
        return apartment;
    }

    public String getFloor() {
        return floor;
    }

    public String getComment() {
        return comment;
    }

    public boolean isFavorite() {
        return favorite;
    }

    public String getFullAddress() {
        String s = street;
        if (!house.isEmpty())
            s += ", " + house;
        if (!apartment.isEmpty())
            s += " - " + apartment;
        if (!floor.isEmpty())
            s += ", " +  floor;
        return s;
    }

    // endregion

    // region ==================== Setters ====================

    public void setId(String id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public void setHouse(String house) {
        this.house = house;
    }

    public void setApartment(String apartment) {
        this.apartment = apartment;
    }

    public void setFloor(String floor) {
        this.floor = floor;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public void setFavorite(boolean favorite) {
        this.favorite = favorite;
    }

    // endregion

    // region ==================== Parcelable ====================

    public AddressDto(Parcel in) {
        this.id = in.readString();
        this.name = in.readString();
        this.street = in.readString();
        this.house = in.readString();
        this.apartment = in.readString();
        this.floor = in.readString();
        this.comment = in.readString();
        this.favorite = in.readInt() == 1;
    }

    public static final Creator<AddressDto> CREATOR = new Creator<AddressDto>() {
        @Override
        public AddressDto createFromParcel(Parcel in) {
            return new AddressDto(in);
        }

        @Override
        public AddressDto[] newArray(int size) {
            return new AddressDto[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(id);
        parcel.writeString(name);
        parcel.writeString(street);
        parcel.writeString(house);
        parcel.writeString(apartment);
        parcel.writeString(floor);
        parcel.writeString(comment);
        if (favorite) {
            parcel.writeInt(1);
        } else {
            parcel.writeInt(0);
        }
    }

    // endregion
}