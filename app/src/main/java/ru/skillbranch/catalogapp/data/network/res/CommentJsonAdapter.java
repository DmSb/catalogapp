package ru.skillbranch.catalogapp.data.network.res;

import com.squareup.moshi.FromJson;
import com.squareup.moshi.ToJson;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class CommentJsonAdapter {
    @FromJson CommentRes commentFromJson(CommentJson commentJson) {
        CommentRes commentRes = new CommentRes();
        if (commentJson.getCommentDate() != null) {
            DateFormat format = new SimpleDateFormat("yyyy-MM-dd\'T\'HH:mm:ss.SSS\'Z\'", Locale.ENGLISH);
            try {
                Date date = format.parse(commentJson.getCommentDate());
                commentRes.setCommentDate(date);
            } catch (ParseException e) {
                e.printStackTrace();
                return null;
            }
        } else {
            commentRes.setCommentDate(new Date());
        }
        commentRes.setId(commentJson.getId());
        commentRes.setAvatar(commentJson.getAvatar());
        commentRes.setUserName(commentJson.getUserName());
        commentRes.setRating(commentJson.getRating());
        commentRes.setComment(commentJson.getComment());
        commentRes.setActive(commentJson.isActive());
        return commentRes;
    }

    @ToJson CommentJson commentToJson(CommentRes commentRes) {
        CommentJson commentJson = new CommentJson();
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd\'T\'HH:mm:ss.SSS\'Z\'", Locale.ENGLISH);
        commentJson.setId(commentRes.getId());
        commentJson.setAvatar(commentRes.getAvatar());
        commentJson.setUserName(commentRes.getUserName());
        commentJson.setRating(commentRes.getRating());
        commentJson.setCommentDate(format.format(commentRes.getCommentDate()));
        commentJson.setComment(commentRes.getComment());
        commentJson.setActive(commentRes.isActive());
        return commentJson;
    }
}
