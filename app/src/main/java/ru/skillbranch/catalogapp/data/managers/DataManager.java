package ru.skillbranch.catalogapp.data.managers;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import retrofit2.Retrofit;
import ru.skillbranch.catalogapp.App;
import ru.skillbranch.catalogapp.data.dto.AddressDto;
import ru.skillbranch.catalogapp.data.dto.SettingsDto;
import ru.skillbranch.catalogapp.data.dto.UserDto;
import ru.skillbranch.catalogapp.data.network.RestCallTransformer;
import ru.skillbranch.catalogapp.data.network.RestService;
import ru.skillbranch.catalogapp.data.network.res.ProductRes;
import ru.skillbranch.catalogapp.data.storage.ProductRealm;
import ru.skillbranch.catalogapp.data.storage.RealmManager;
import ru.skillbranch.catalogapp.di.DaggerService;
import ru.skillbranch.catalogapp.di.modules.LocalModule;
import ru.skillbranch.catalogapp.di.modules.NetworkModule;
import rx.Observable;
import rx.schedulers.Schedulers;

public class DataManager {

    private static DataManager sInstance = new DataManager();

    private UserDto mUser;
    //private ArrayList<AddressDto> mAddressList;
    private SettingsDto mSetting;

    @Inject
    PreferencesManager mPreferencesManager;
    @Inject
    RestService mRestService;
    @Inject
    Retrofit mRetrofit;
    @Inject
    RealmManager mRealmManager;

    private DataManager() {
        createDaggerComponent();
        loadUserInfo();
    }

    public static DataManager getInstance() {
        return sInstance;
    }

    public Retrofit getRetrofit() {
        return mRetrofit;
    }

    // region ==================== UserDto ====================

    private void loadUserInfo() {
        mUser = new UserDto(
                mPreferencesManager.getUserName(),
                mPreferencesManager.getUserAvatar(),
                mPreferencesManager.getUserPhone());
        mSetting = new SettingsDto(mPreferencesManager.getUserOrderNotify(),
                mPreferencesManager.getUserPromoNotify());
        //mAddressList = getAddressList();
    }

    public void saveUserProfileInfo() {
        mPreferencesManager.saveUserName(mUser.getFullName());
        mPreferencesManager.saveUserPhone(mUser.getPhone());
        mPreferencesManager.saveUserAvatar(mUser.getAvatar());
    }

    public UserDto getUser() {
        return mUser;
    }

    public void saveToken(String token) {
        mPreferencesManager.saveToken(token);
    }

    public boolean isAuthUser() {
        return ! mPreferencesManager.getToken().isEmpty();
    }

    // endregion

    // region ==================== AddressDto ====================

    public ArrayList<AddressDto> getAddressList() {
        return mRealmManager.getAddressesFromRealm();
    }

    // endregion

    // region ==================== Setting ====================

    public SettingsDto getSetting() {
        return mSetting;
    }

    public void setOrderNotification(boolean isChecked) {
        mSetting.setOrderNotification(isChecked);
        mPreferencesManager.saveUserOrderNotify(isChecked);
    }

    public void setPromoNotification(boolean isChecked) {
        mSetting.setPromoNotification(isChecked);
        mPreferencesManager.saveUserPromoNotify(isChecked);
    }

    // endregion

    // region ==================== Product ====================

    public Observable<ProductRealm>  getProductObsFromNetwork() {
        return mRestService.getProductResObs(mPreferencesManager.getLastProductUpdate())
                .compose(new RestCallTransformer<List<ProductRes>>()) // трансформируем response
                .flatMap(Observable::from) // преобразуем спиков товаров в последовательность товаров
                .subscribeOn(Schedulers.io())
                .doOnNext(productRes -> {
                    if (!productRes.isActive()) {
                        deleteProductFromDb(productRes);
                    }
                })
                .filter(ProductRes::isActive) // пропускаем дальше только активные
                .doOnNext(this::saveProductToDb)
                .flatMap(productRes -> Observable.empty());
    }

    public Observable<ProductRealm> getProductFromRealm() {
        return mRealmManager.getAllProductsFromRealm();
    }

    private void saveProductToDb(ProductRes productRes) {
        mRealmManager.saveProductToRealm(productRes);
    }

    private void deleteProductFromDb(ProductRes productRes) {
        mRealmManager.deleteFromRealm(ProductRealm.class, productRes.getId());
    }

    public void saveLastProductUpdate(String lastModified) {
        mPreferencesManager.saveLastProductUpdate(lastModified);
    }

    // endregion

    // region ==================== Dagger ====================

    @Singleton
    @dagger.Component(dependencies = App.AppComponent.class,
            modules =  {LocalModule.class, NetworkModule.class})
    public interface Component {
        void inject(DataManager dataManager);
    }

    private void createDaggerComponent() {
        DaggerService.createComponent(Component.class,
                DaggerDataManager_Component.class,
                App.getAppComponent(),
                new LocalModule(),
                new NetworkModule()).inject(this);
    }

    // endregion
}