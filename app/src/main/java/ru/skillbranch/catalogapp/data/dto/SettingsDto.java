package ru.skillbranch.catalogapp.data.dto;

import android.os.Parcel;
import android.os.Parcelable;

public class SettingsDto implements Parcelable {
    private boolean orderNotification;
    private boolean promoNotification;

    public SettingsDto(boolean orderNotification, boolean promoNotification) {
        this.orderNotification = orderNotification;
        this.promoNotification = promoNotification;
    }

    public boolean isOrderNotification() {
        return orderNotification;
    }

    public boolean isPromoNotification() {
        return promoNotification;
    }

    public void setOrderNotification(boolean orderNotification) {
        this.orderNotification = orderNotification;
    }

    public void setPromoNotification(boolean promoNotification) {
        this.promoNotification = promoNotification;
    }

    // region ==================== Parcelable ====================

    protected SettingsDto(Parcel in) {
        this.orderNotification = Boolean.valueOf(in.readString());
        this.promoNotification = Boolean.valueOf(in.readString());
    }

    public static final Creator<SettingsDto> CREATOR = new Creator<SettingsDto>() {
        @Override
        public SettingsDto createFromParcel(Parcel in) {
            return new SettingsDto(in);
        }

        @Override
        public SettingsDto[] newArray(int size) {
            return new SettingsDto[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(String.valueOf(orderNotification));
        parcel.writeString(String.valueOf(promoNotification));
    }

    // endregion
}
