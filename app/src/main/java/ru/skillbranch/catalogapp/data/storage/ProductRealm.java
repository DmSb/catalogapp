package ru.skillbranch.catalogapp.data.storage;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import ru.skillbranch.catalogapp.data.network.res.CommentRes;
import ru.skillbranch.catalogapp.data.network.res.ProductRes;

public class ProductRealm extends RealmObject {
    @PrimaryKey
    private String id;
    private String productName;
    private String imageUrl;
    private String description;
    private int price;
    private int count = 0;
    private boolean favorite;
    private float rating;
    private RealmList<CommentRealm> comments = new RealmList<>();

    public ProductRealm() {
    }

    public ProductRealm(ProductRes productRes) {
        id = productRes.getId();
        productName = productRes.getProductName();
        imageUrl = productRes.getImageUrl();
        description = productRes.getDescription();
        price = productRes.getPrice();
        rating = productRes.getRating();
        comments = new RealmList<>();

        if (productRes.getComments().size() > 0) {
            for (CommentRes commentRes: productRes.getComments()) {
                comments.add(new CommentRealm(commentRes));
            }
        }
    }

    // region ==================== Getters ====================

    public String getId() {
        return id;
    }

    public String getProductName() {
        return productName;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public String getDescription() {
        return description;
    }

    public int getPrice() {
        return price;
    }

    public int getCount() {
        return count;
    }

    public boolean isFavorite() {
        return favorite;
    }

    public float getRating() {
        return rating;
    }

    public RealmList<CommentRealm> getComments() {
        return comments;
    }

    // endregion

    // region ==================== Setters ====================

    public void addProduct() {
        count++;
    }

    public void deleteProduct() {
        count--;
    }

    public void changeFavorite() {
        setFavorite(!this.favorite);
    }

    private void setFavorite(boolean favorite) {
        this.favorite = favorite;
    }

    // endregion
}