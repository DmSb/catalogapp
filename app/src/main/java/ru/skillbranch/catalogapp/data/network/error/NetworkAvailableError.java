package ru.skillbranch.catalogapp.data.network.error;

public class NetworkAvailableError extends Throwable {

    public NetworkAvailableError(){
        super("Интренет недоступен, попробуйте позже");
    }
}
