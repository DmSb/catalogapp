package ru.skillbranch.catalogapp.data.managers;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class PreferencesManager {

    private final static String TOKEN_KEY = "TOKEN_KEY";
    private final static String USER_NAME_KEY = "USER_NAME_KEY";
    private final static String USER_EMAIL_KEY = "USER_EMAIL_KEY";
    private final static String USER_PHONE_KEY = "USER_PHONE_KEY";
    private final static String USER_AVATAR_KEY = "USER_AVATAR_KEY";
    private final static String USER_ORDER_KEY = "USER_ORDER_KEY";
    private final static String USER_PROMO_KEY = "USER_PROMO_KEY";

    private final static String PRODUCT_LAST_UPDATE_KEY = "PRODUCT_LAST_UPDATE_KEY";

    private final SharedPreferences mSharedPreferences;

    public PreferencesManager(Context context) {
        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
    }

    // region ==================== User ====================

    String getToken() {
        return mSharedPreferences.getString(TOKEN_KEY, "");
    }

    String getUserName() {
        return mSharedPreferences.getString(USER_NAME_KEY, "");
    }

    String getUserEmail() {
        return mSharedPreferences.getString(USER_EMAIL_KEY, "");
    }

    String getUserPhone() {
        return mSharedPreferences.getString(USER_PHONE_KEY, "");
    }

    String getUserAvatar() {
        return mSharedPreferences.getString(USER_AVATAR_KEY, "");
    }

    boolean getUserOrderNotify() {
        return mSharedPreferences.getBoolean(USER_ORDER_KEY, false);
    }

    boolean getUserPromoNotify() {
        return mSharedPreferences.getBoolean(USER_PROMO_KEY, false);
    }

    void saveToken(String token) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(TOKEN_KEY, token);
        editor.apply();
    }

    void saveUserName(String userName) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(USER_NAME_KEY, userName);
        editor.apply();
    }

    void saveUserEmail(String userEmail) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(USER_EMAIL_KEY, userEmail);
        editor.apply();
    }

    void saveUserPhone(String userPhone) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(USER_PHONE_KEY, userPhone);
        editor.apply();
    }

    void saveUserAvatar(String userAvatar) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(USER_AVATAR_KEY, userAvatar);
        editor.apply();
    }

    void saveUserOrderNotify(boolean userOrderNotify) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putBoolean(USER_ORDER_KEY, userOrderNotify);
        editor.apply();
    }

    void saveUserPromoNotify(boolean userPromoNotify) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putBoolean(USER_PROMO_KEY, userPromoNotify);
        editor.apply();
    }

    // endregion

    // region ==================== Product ====================

    String getLastProductUpdate() {
        return mSharedPreferences.getString(PRODUCT_LAST_UPDATE_KEY, "Thu, 01 Jan 1970 00:00:00 GMT");
    }

    void saveLastProductUpdate(String lastProductUpdate) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(PRODUCT_LAST_UPDATE_KEY, lastProductUpdate);
        editor.apply();
    }
}