package ru.skillbranch.catalogapp.data.network;

import java.util.List;

import retrofit2.Response;
import retrofit2.http.GET;
import retrofit2.http.Header;
import ru.skillbranch.catalogapp.data.network.res.ProductRes;
import ru.skillbranch.catalogapp.utils.ConstantManager;
import rx.Observable;

public interface RestService {
    //@GET("error/400")
    @GET("products")
    Observable<Response<List<ProductRes>>> getProductResObs(
            @Header(ConstantManager.IF_MODIFIED_SINCE_HEADER) String lastEntityUpdate);
}
