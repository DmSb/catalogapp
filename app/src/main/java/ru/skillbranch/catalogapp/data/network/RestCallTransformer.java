package ru.skillbranch.catalogapp.data.network;

import com.fernandocejas.frodo.annotation.RxLogObservable;

import retrofit2.Response;
import ru.skillbranch.catalogapp.data.managers.DataManager;
import ru.skillbranch.catalogapp.data.network.error.ErrorUtils;
import ru.skillbranch.catalogapp.data.network.error.NetworkAvailableError;
import ru.skillbranch.catalogapp.utils.ConstantManager;
import rx.Observable;

public class RestCallTransformer<R> implements Observable.Transformer<Response<R>, R> {

    @Override
    @RxLogObservable
    public Observable<R> call(Observable<Response<R>> responseObservable) {

        return NetworkStatusChecker.isInternetAvailable()
                .flatMap(aBoolean -> aBoolean ? responseObservable : Observable.error(new NetworkAvailableError()))
                .flatMap(rResponse -> {
                    switch (rResponse.code()) {
                        case 200:
                            String lastModified = rResponse.headers().get(ConstantManager.LAST_MODIFIED_HEADER);
                            if (lastModified != null) {
                                DataManager.getInstance().saveLastProductUpdate(lastModified);
                            }
                            return Observable.just(rResponse.body()); // пришли измененные данные с сервера
                        case 304:
                            return Observable.empty();  // с сервера данные не пришли, не было изменений
                        default:
                            return Observable.error(ErrorUtils.parseError(rResponse));
                    }
                });
    }
}
