package ru.skillbranch.catalogapp.data.network.res;

import com.squareup.moshi.Json;
import com.squareup.moshi.JsonAdapter;
import com.squareup.moshi.Moshi;

import java.io.IOException;
import java.util.List;

public class ProductRes {
    @Json(name = "_id")
    private String id;
    private int remoteId;
    private String productName;
    private String imageUrl;
    private String description;
    private int price;
    @Json(name = "raiting")
    private float rating;
    private boolean active;
    private List<CommentRes> comments = null;

    public ProductRes(int remoteId, String productName, String imageUrl, String description,
                      int price, float rating, boolean active, List<CommentRes> comments) {
        this.remoteId = remoteId;
        this.productName = productName;
        this.imageUrl = imageUrl;
        this.description = description;
        this.price = price;
        this.rating = rating;
        this.active = active;
        this.comments = comments;
    }

    // region ==================== Getters ====================

    public String getId() {
        return id;
    }

    public int getRemoteId() {
        return remoteId;
    }

    public String getProductName() {
        return productName;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public String getDescription() {
        return description;
    }

    public int getPrice() {
        return price;
    }

    public float getRating() {
        return rating;
    }

    public boolean isActive() {
        return active;
    }

    public List<CommentRes> getComments() {
        return comments;
    }

    // endregion

    // region ==================== Json converter ====================

    public String toJson() {
        Moshi moshi = new Moshi.Builder().build();
        JsonAdapter<ProductRes> jsonAdapter = moshi.adapter(ProductRes.class);
        return jsonAdapter.toJson(this);
    }

    public static ProductRes fromJson(String jsonValue) {
        Moshi moshi = new Moshi.Builder().build();
        JsonAdapter<ProductRes> jsonAdapter = moshi.adapter(ProductRes.class);
        try {
            return jsonAdapter.fromJson(jsonValue);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    // endregion
}