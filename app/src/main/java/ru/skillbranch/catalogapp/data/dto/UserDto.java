package ru.skillbranch.catalogapp.data.dto;

import android.os.Parcel;
import android.os.Parcelable;

public class UserDto implements Parcelable {

    private String fullName;
    private String avatar;
    private String phone;

    public UserDto() {
    }

    public UserDto(String fullName, String avatar, String phone) {
        this.fullName = fullName;
        this.avatar = avatar;
        this.phone = phone;
    }

    // region ==================== Getters ====================

    public String getFullName() {
        return fullName;
    }

    public String getAvatar() {
        return avatar;
    }

    public String getPhone() {
        return phone;
    }

    // endregion

    // region ==================== Setters ====================

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    // endregion

    // region ==================== Parcelable ====================

    protected UserDto(Parcel in) {
        this.fullName = in.readString();
        this.avatar = in.readString();
        this.phone = in.readString();
    }

    public static final Creator<UserDto> CREATOR = new Creator<UserDto>() {
        @Override
        public UserDto createFromParcel(Parcel in) {
            return new UserDto(in);
        }

        @Override
        public UserDto[] newArray(int size) {
            return new UserDto[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(fullName);
        parcel.writeString(avatar);
        parcel.writeString(phone);
    }

    // endregion
}