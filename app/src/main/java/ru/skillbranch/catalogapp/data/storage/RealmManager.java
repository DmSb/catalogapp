package ru.skillbranch.catalogapp.data.storage;

import java.util.ArrayList;

import io.realm.Realm;
import io.realm.RealmObject;
import io.realm.RealmResults;
import ru.skillbranch.catalogapp.data.dto.AddressDto;
import ru.skillbranch.catalogapp.data.network.res.CommentRes;
import ru.skillbranch.catalogapp.data.network.res.ProductRes;
import rx.Observable;

public class RealmManager {

    private Realm mRealmInstance;

    // region ==================== ProductRealm ====================

    public void saveProductToRealm(ProductRes productRes) {
        Realm realm = Realm.getDefaultInstance();
        ProductRealm productRealm = new ProductRealm(productRes);
        if (!productRealm.getComments().isEmpty()) {
            Observable.from(productRes.getComments())
                    .doOnNext(commentRes -> {
                        if (!commentRes.isActive()) {
                            deleteFromRealm(CommentRealm.class, commentRes.getId()); // Удаляем неактивные
                        }})
                    .filter(CommentRes::isActive)
                    .map(CommentRealm::new) // преобразовываем в RealmObject
                    .subscribe(commentRealm -> productRealm.getComments().add(commentRealm));
        }
        realm.executeTransaction(realm1 -> realm1.insertOrUpdate(productRealm));
        realm.close();
    }

    public void deleteFromRealm(Class<? extends RealmObject> entityRealmClass, String id) {
        Realm realm = Realm.getDefaultInstance();
        RealmObject entity = realm.where(entityRealmClass).equalTo("id", id).findFirst();

        if (entity != null) {
            realm.executeTransaction(realm1 -> entity.deleteFromRealm()); // удаляем из базы
            realm.close();
        }
    }

    public Observable<ProductRealm> getAllProductsFromRealm() {
        RealmResults<ProductRealm> managedProduct = getQueryRealmInstance()
                .where(ProductRealm.class)
                .findAllAsync();

        return managedProduct
                .asObservable() // получаем RealmResult как Observable
                .filter(RealmResults::isLoaded) // получаем только загруженнеые результаты (горячий observable)
                //.first() // если нужна холодный observable
                .flatMap(Observable::from); // преобразуем в Observable<ProductRealm>
    }

    // endregion

    // region ==================== AddressRealm ====================

    public ArrayList<AddressDto> getAddressesFromRealm() {
        Realm realm = Realm.getDefaultInstance();
        RealmResults<AddressRealm> addressResults = realm
                .where(AddressRealm.class)
                .findAll();

        ArrayList<AddressDto> addressList = new ArrayList<>();

        for (AddressRealm addressRealm : addressResults) {
            addressList.add(new AddressDto(addressRealm));
        }

        return addressList;
    }

    // endregion



    private Realm getQueryRealmInstance() {
        if (mRealmInstance == null || mRealmInstance.isClosed()) {
            mRealmInstance = Realm.getDefaultInstance();
        }
        return mRealmInstance;
    }
}