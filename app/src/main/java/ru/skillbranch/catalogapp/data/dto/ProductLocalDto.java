package ru.skillbranch.catalogapp.data.dto;

import android.support.annotation.Nullable;

import com.squareup.moshi.JsonAdapter;
import com.squareup.moshi.Moshi;

import java.io.IOException;

public class ProductLocalDto {
    private String id;
    private boolean favorite;
    private int count;

    public ProductLocalDto() {
    }

    public ProductLocalDto(String id, boolean favorite, int count) {
        this.id = id;
        this.favorite = favorite;
        this.count = count;
    }

    // region ==================== Json convert ====================

    @Nullable
    public static ProductLocalDto fromJson(String jsonValue) {
        Moshi moshi = new Moshi.Builder().build();
        JsonAdapter<ProductLocalDto> jsonAdapter = moshi.adapter(ProductLocalDto.class);
        try {
            return jsonAdapter.fromJson(jsonValue);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public String toJson() {
        Moshi moshi = new Moshi.Builder().build();
        JsonAdapter<ProductLocalDto> jsonAdapter = moshi.adapter(ProductLocalDto.class);
        return jsonAdapter.toJson(this);
    }

    // endregion

    // region ==================== Gettres ====================

    public String getId() {
        return id;
    }

    boolean isFavorite() {
        return favorite;
    }

    public int getCount() {
        return count;
    }

    public void addCount() {
        count++;
    }

    public void deleteCount() {
        count--;
    }

    public void setId(String id) {
        this.id = id;
    }

    // endregion


}