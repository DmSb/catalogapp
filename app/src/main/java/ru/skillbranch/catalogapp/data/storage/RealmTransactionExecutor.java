package ru.skillbranch.catalogapp.data.storage;

import io.realm.Realm;

public class RealmTransactionExecutor {
    public static void executeRealmTransaction(Realm.Transaction transaction) {
        Realm realm = Realm.getDefaultInstance();
        realm.executeTransaction(transaction);
        if (realm.isInTransaction())
            realm.close();
    }
}
