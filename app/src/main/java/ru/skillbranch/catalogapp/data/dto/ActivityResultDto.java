package ru.skillbranch.catalogapp.data.dto;

import android.content.Intent;
import android.support.annotation.Nullable;

public class ActivityResultDto {
    private int requestCode;
    private int resultCode;
    @Nullable
    private Intent intent;

    public ActivityResultDto(int requestCode, int resultCode, Intent intent) {
        this.requestCode = requestCode;
        this.resultCode = resultCode;
        this.intent = intent;
    }

    public int getRequestCode() {
        return requestCode;
    }

    public int getResultCode() {
        return resultCode;
    }

    @Nullable
    public Intent getIntent() {
        return intent;
    }
}
