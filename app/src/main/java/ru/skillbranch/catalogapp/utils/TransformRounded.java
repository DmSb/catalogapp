package ru.skillbranch.catalogapp.utils;

import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Shader;
import android.support.v4.graphics.ColorUtils;

import com.squareup.picasso.Transformation;

public class TransformRounded implements Transformation {

    @Override
    public Bitmap transform(final Bitmap source) {
        final Paint paint = new Paint();
        paint.setAntiAlias(true);
        paint.setShader(new BitmapShader(source, Shader.TileMode.CLAMP, Shader.TileMode.CLAMP));

        final Paint border = new Paint();
        border.setColor(ColorUtils.setAlphaComponent(Color.WHITE, 0xFF));
        border.setAntiAlias(true);

        final Bitmap output = Bitmap.createBitmap(source.getWidth(), source.getHeight(), Bitmap.Config.ARGB_8888);
        final Canvas canvas = new Canvas(output);

        // draw white circle
        int borderRadius = 3;
        canvas.drawCircle(source.getWidth() / 2, source.getHeight() / 2, source.getWidth() / 2, border);

        // draw image
        canvas.drawCircle(source.getWidth() / 2, source.getHeight() / 2, source.getWidth() / 2 - borderRadius, paint);

        if (source != output)
            source.recycle();

        return output;
    }

    @Override
    public String key() {
        return "circle";
    }
}