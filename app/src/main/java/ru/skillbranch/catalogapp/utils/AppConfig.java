package ru.skillbranch.catalogapp.utils;

public interface AppConfig {
    String BASE_URL = "http://skba1.mgbeta.ru/api/v1/";
    int MAX_CONNECTION_TIMEOUT = 5000;
    int MAX_READ_TIMEOUT = 5000;
    int MAX_WRITE_TIMEOUT = 5000;
}
