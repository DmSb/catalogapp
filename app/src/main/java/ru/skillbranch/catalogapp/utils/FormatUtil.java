package ru.skillbranch.catalogapp.utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Locale;

public class FormatUtil {
    private static DateFormat sLongDateFormat;

    public static void initFormat() {
        sLongDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.ENGLISH);
    }
}
