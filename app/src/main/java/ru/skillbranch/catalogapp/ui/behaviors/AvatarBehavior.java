package ru.skillbranch.catalogapp.ui.behaviors;

import android.content.Context;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import ru.skillbranch.catalogapp.R;

public class AvatarBehavior  extends CoordinatorLayout.Behavior<ImageView> {
    private Context mContext;

    public AvatarBehavior(Context context, AttributeSet attrs) {
        super(context, attrs);
        mContext = context;
    }

    @Override
    public boolean onDependentViewChanged(CoordinatorLayout parent, ImageView child, View dependency) {
        if (dependency instanceof AppBarLayout) {
            int height = mContext.getResources().getDimensionPixelSize(R.dimen.size_account_avatar_136);

            if (Math.abs(dependency.getTop()) > height / 2) {
                child.setVisibility(View.GONE);
            } else {
                ViewGroup.LayoutParams params = child.getLayoutParams();
                child.setVisibility(View.VISIBLE);
                params.height = height + dependency.getTop();
                params.width = height + dependency.getTop();
                child.setLayoutParams(params);
            }
        }
        return true;
    }

    @Override
    public boolean layoutDependsOn(CoordinatorLayout parent, ImageView child, View dependency) {
        return dependency instanceof AppBarLayout;
    }
}
