package ru.skillbranch.catalogapp.ui.validators;

import android.content.Context;
import android.support.design.widget.TextInputLayout;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.widget.EditText;

import ru.skillbranch.catalogapp.utils.ConstantManager;

public class PhoneValidator implements TextWatcher {

    private final Context mContext;
    private final EditText mEdit;
    private final TextInputLayout mEditLayout;
    private final int mMessageResId;
    private boolean mIsValid = false;

    private int mPosBefore, mPosAfter;
    private String mValueBef = "";
    private String mValue = "";

    public PhoneValidator(Context context, EditText edit, TextInputLayout editLayout, int resId) {
        mContext = context;
        mEdit = edit;
        mEditLayout = editLayout;
        mMessageResId = resId;
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void afterTextChanged(Editable editable) {
        if (mEdit != null) {
            String phone = editable.toString();

            Boolean endPosition = false;

            mEdit.removeTextChangedListener(this);
            mPosAfter = mEdit.getSelectionStart();

            if (mPosAfter == mEdit.getText().length()) {
                endPosition = true;
            }

            phone = preparePhone(phone, endPosition);

            mEdit.setText(phone);

            mEdit.addTextChangedListener(this);
            if (endPosition) {
                mEdit.setSelection(mEdit.getText().length());
            } else {
                mEdit.setSelection(mPosAfter);
            }

            mIsValid = validatePhone(phone);
            if (mIsValid) {
                hideError();
            } else {
                showError();
            }
        }
    }

    private String preparePhone(String phone, boolean endPosition) {
        mValue = "";
        String newVal = "";
        int i;
        int dropSymbol = -1;

        if ((mPosBefore > mPosAfter) && (!endPosition)) {
            if (! Character.isDigit(mValueBef.charAt(mPosAfter))) {
                mPosAfter--;
                dropSymbol = mPosAfter;
            }
        }

        if (phone != null && phone.length() > 0) {
            for (i = 0; i < phone.length(); i++) {
                if ((Character.isDigit(phone.charAt(i))) && (i != dropSymbol)) {
                    mValue = mValue + phone.charAt(i);
                }
            }

            if (mValue != null && mValue.length() > 0) {
                if (mValue.charAt(0) == '8') {
                    mValue = '7' + mValue.substring(1, mValue.length());
                }

                int l = mValue.length();
                if (l > 20) {
                    l = 20;
                }

                for (i = 0; i < mValue.length(); i++) {
                    switch (i) {
                        case 0:
                            newVal = "+" + mValue.charAt(i);
                            l++;
                            break;
                        case 1:
                        case 4:
                            newVal = newVal + " " + mValue.charAt(i);
                            l++;
                            break;
                        case 7:
                        case 9:
                            newVal = newVal + "-" + mValue.charAt(i);
                            l++;
                            break;
                        default:
                            newVal = newVal + mValue.charAt(i);
                    }
                }

                if (mPosAfter < 0)
                    mPosAfter = 0;

                if (l > 20) {
                    l = 20;
                }
                return newVal.substring(0, l);
            } else {
                return newVal;
            }

        } else {
            return newVal;
        }
    }

    private boolean validatePhone(String phone) {
        return !TextUtils.isEmpty(phone) &&
                mValue.length() >= 11 &&
                phone.matches(ConstantManager.PATTERN_PHONE);
    }

    private void showError() {
        if (mEditLayout != null) {
            mEditLayout.setErrorEnabled(true);
            mEditLayout.setError(mContext.getString(mMessageResId));
        }
        mIsValid = false;
    }

    private void hideError() {
        if (mEditLayout != null) {
            mEditLayout.setErrorEnabled(false);
            mEditLayout.setError("");
        }
        mIsValid = true;
    }
}