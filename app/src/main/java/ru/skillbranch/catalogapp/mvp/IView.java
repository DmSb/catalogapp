package ru.skillbranch.catalogapp.mvp;

public interface IView {

    boolean viewOnBackPressed();
}
