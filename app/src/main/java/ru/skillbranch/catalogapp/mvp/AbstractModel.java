package ru.skillbranch.catalogapp.mvp;

import javax.inject.Inject;

import ru.skillbranch.catalogapp.data.managers.DataManager;
import ru.skillbranch.catalogapp.di.DaggerService;
import ru.skillbranch.catalogapp.di.components.DaggerDataManagerComponent;
import ru.skillbranch.catalogapp.di.components.DataManagerComponent;
import ru.skillbranch.catalogapp.di.modules.DataManagerModule;

public abstract class AbstractModel {

    @Inject
    public DataManager mDataManager;

    public AbstractModel() {
        createDaggerComponent();
    }

    // region ==================== Dagger ====================

    private void createDaggerComponent() {
        DaggerService.createComponent(DataManagerComponent.class,
                DaggerDataManagerComponent.class,
                new DataManagerModule()).inject(this);
    }

    // endregion
}