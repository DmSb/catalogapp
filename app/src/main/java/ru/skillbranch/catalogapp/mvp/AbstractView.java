package ru.skillbranch.catalogapp.mvp;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.FrameLayout;

import javax.inject.Inject;

public abstract class AbstractView<P extends AbstractPresenter> extends FrameLayout implements IView {
    @Inject
    protected P mPresenter;

    public AbstractView(Context context, AttributeSet attrs) {
        super(context, attrs);
        if (!isInEditMode()) {
            initDagger(context);
        }
    }

    protected abstract void initDagger(Context context);

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        if (!isInEditMode()){
            initBinding();
        }
    }

    protected abstract void initBinding();

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (!isInEditMode()){
            mPresenter.takeView(this);
            attachListeners();
        }
    }

    protected abstract void attachListeners();

    @Override
    protected void onDetachedFromWindow() {
        if (!isInEditMode()){
            detachListeners();
            mPresenter.dropView(this);
        }
        super.onDetachedFromWindow();
    }

    protected abstract void detachListeners();
}
