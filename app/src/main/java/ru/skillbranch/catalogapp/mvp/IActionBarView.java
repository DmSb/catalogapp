package ru.skillbranch.catalogapp.mvp;

import android.support.v4.view.ViewPager;

public interface IActionBarView {
    void setBarTitle(CharSequence title);
    void setBarVisible(boolean visible);
    void setBackArrow(boolean enabled);
    void setTabLayout(ViewPager pager);
    void removeTabLayout();
    void setProductCardVisible(boolean visible);
    void setFab(int fabResId);
}
