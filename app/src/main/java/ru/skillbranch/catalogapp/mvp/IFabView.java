package ru.skillbranch.catalogapp.mvp;

import android.view.View;

public interface IFabView {
    void setFabVisible(boolean visible);
    void setImageResId(int imageResId);
    void setFabListener(View.OnClickListener listener);
}
