package ru.skillbranch.catalogapp;

import android.app.Application;
import android.content.Context;
import android.databinding.BindingAdapter;
import android.net.Uri;
import android.widget.ImageView;

import com.squareup.leakcanary.LeakCanary;
import com.squareup.leakcanary.RefWatcher;
import com.squareup.picasso.Picasso;

import dagger.Provides;
import io.realm.Realm;
import mortar.MortarScope;
import mortar.bundler.BundleServiceRunner;
import ru.skillbranch.catalogapp.di.DaggerService;
import ru.skillbranch.catalogapp.di.modules.PicassoCacheModule;
import ru.skillbranch.catalogapp.features.root.DaggerRootActivity_RootComponent;
import ru.skillbranch.catalogapp.features.root.RootActivity;
import ru.skillbranch.catalogapp.features.root.RootModule;
import ru.skillbranch.catalogapp.mortar.ScreenScoper;
import ru.skillbranch.catalogapp.utils.FontUtil;
import ru.skillbranch.catalogapp.utils.FormatUtil;
import ru.skillbranch.catalogapp.utils.TransformRounded;

public class  App extends Application {

    private static AppComponent sAppComponent;
    private static Context sContext;

    private MortarScope mRootScope;
    private MortarScope mRootActivityScope;
    private static RootActivity.RootComponent mRootActivityComponent;
    private RefWatcher mRefWatcher;

    @Override
    public void onCreate() {
        super.onCreate();

        Realm.init(this);

        mRefWatcher = LeakCanary.install(this);

        FontUtil.initFont(this);
        FormatUtil.initFormat();

        createAppComponent();
        createRootActivityComponent();
        createMortarScope();

        ScreenScoper.registerScope(mRootScope);
        ScreenScoper.registerScope(mRootActivityScope);

        sContext = getApplicationContext();
    }

    @Override
    public Object getSystemService(String name) {
        if (mRootScope != null) {
            return mRootScope.hasService(name) ? mRootScope.getService(name) : super.getSystemService(name);
        } else {
            return super.getSystemService(name);
        }
    }

    public static AppComponent getAppComponent() {
        return sAppComponent;
    }

    public static Context getContext() {
        return sContext;
    }

    // region ==================== Mortar ====================

    private void createMortarScope() {
        mRootScope = MortarScope.buildRootScope()
                .withService(DaggerService.SERVICE_NAME, sAppComponent)
                .build("Root");
        mRootActivityScope = mRootScope.buildChild()
                .withService(DaggerService.SERVICE_NAME, mRootActivityComponent)
                .withService(BundleServiceRunner.SERVICE_NAME, new BundleServiceRunner())
                .build(RootActivity.class.getName());
    }

    // endregion

    // region ==================== Dagger ====================

    private void createAppComponent() {
        sAppComponent = DaggerApp_AppComponent.builder()
                .appModule(new AppModule(getApplicationContext(), mRefWatcher))
                .build();
    }

    private void createRootActivityComponent() {
        mRootActivityComponent = DaggerService.createComponent(RootActivity.RootComponent.class,
                DaggerRootActivity_RootComponent.class,
                getAppComponent(),
                new PicassoCacheModule(),
                new RootModule());
    }

    public static RootActivity.RootComponent getRootActivityComponent() {
        return mRootActivityComponent;
    }

    public static Picasso getPicasso() {
        return mRootActivityComponent.getPicasso();
    }

    @dagger.Module
    public class AppModule {

        private Context mContext;
        private RefWatcher mRefWatcher;

        AppModule(Context context, RefWatcher refWatcher) {
            mContext = context;
            mRefWatcher = refWatcher;
        }

        @Provides
        Context provideContext () {
            return mContext;
        }

        @Provides
        RefWatcher provideRefWatcher() {
            return mRefWatcher;
        }
    }

    @dagger.Component(modules = AppModule.class)
    public interface AppComponent {
        Context getContext();
        RefWatcher getRefWatcher();
    }

    // endregion

    @BindingAdapter({"image_url"})
    public static void loadImage(ImageView imageView, String imageUrl) {
        getPicasso().load(Uri.parse(imageUrl))
                .placeholder(R.drawable.img_bg)
                .into(imageView);
    }

    @BindingAdapter({"image_url_circle_60"})
    public static void loadImageCircle60(ImageView imageView, String imageUrl) {
        try {
            getPicasso().load(Uri.parse(imageUrl))
                    .placeholder(R.drawable.ic_account_circle_135dp)
                    .error(R.drawable.ic_account_circle_135dp)
                    .transform(new TransformRounded())
                    .resize(60, 60)
                    .centerCrop()
                    .into(imageView);
        } catch (NullPointerException e) {
            getPicasso().load(R.drawable.ic_account_circle_135dp)
                    .transform(new TransformRounded())
                    .resize(60, 60)
                    .centerCrop()
                    .into(imageView);
        }
    }
}