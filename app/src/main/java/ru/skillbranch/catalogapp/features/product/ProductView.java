package ru.skillbranch.catalogapp.features.product;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.util.AttributeSet;

import ru.skillbranch.catalogapp.data.dto.ProductDto;
import ru.skillbranch.catalogapp.data.dto.ProductLocalDto;
import ru.skillbranch.catalogapp.databinding.ScreenProductBinding;
import ru.skillbranch.catalogapp.di.DaggerService;
import ru.skillbranch.catalogapp.mvp.AbstractView;
import ru.skillbranch.catalogapp.utils.ConstantManager;

public class ProductView extends AbstractView<ProductPresenter>
        implements IProductView {

    private ScreenProductBinding mBinding;

    public ProductView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    // region ==================== AbstractView ====================

    @Override
    protected void initDagger(Context context) {
        DaggerService.<ProductScreen.ProductComponent>getDaggerComponent(context).inject(this);
    }

    @Override
    protected void initBinding() {
        mBinding = DataBindingUtil.bind(this);
    }

    @Override
    protected void attachListeners() {
        mBinding.minusBtn.setOnClickListener(v -> mPresenter.clickOnMinus());
        mBinding.plusBtn.setOnClickListener(v -> mPresenter.clickOnPlus());
        mBinding.favoriteBtn.setOnClickListener(v -> mPresenter.clickOnFavorite());
        mBinding.showMoreBtn.setOnClickListener(v -> mPresenter.clickOnShowMore());
    }

    @Override
    protected void detachListeners() {
        mBinding.minusBtn.setOnClickListener(null);
        mBinding.plusBtn.setOnClickListener(null);
        mBinding.favoriteBtn.setOnClickListener(null);
        mBinding.showMoreBtn.setOnClickListener(null);
    }

    // endregion

    // region ==================== IProductView ====================

    @Override
    public void showProductView(ProductDto product) {
        mBinding.setProduct(product);

        updateProductCountView(product);

        /*
        mPicasso.load(product.getImageUrl())
                .networkPolicy(NetworkPolicy.OFFLINE)
                .placeholder(R.drawable.img_bg)
                .into(mBinding.productImage, new Callback() {
                    @Override
                    public void onSuccess() {

                    }

                    @Override
                    public void onError() {
                        mPicasso.load(product.getImageUrl())
                                .placeholder(R.drawable.img_bg)
                                .into(mBinding.productImage);
                    }
                });*/
    }

    @Override
    public void updateProductCountView(ProductDto product) {

        mBinding.productCountTxt.setText(String.valueOf(product.getCount()));
        if (product.getCount() > 0) {
            mBinding.productPriceTxt.setText(
                    String.valueOf(product.getCount() * product.getPrice()) + ConstantManager.PRODUCT_COST_SUFFIX);
        } else {
            mBinding.productPriceTxt.setText(
                    String.valueOf(product.getPrice()) + ConstantManager.PRODUCT_COST_SUFFIX);
        }
    }

    @Override
    public void updateFavorite(ProductDto product) {
        mBinding.favoriteBtn.setChecked(product.isFavorite());
    }

    @Override
    public boolean viewOnBackPressed() {
        return false;
    }

    @Override
    public ProductLocalDto getProductLocalInfo() {
        return new ProductLocalDto("",
                mBinding.favoriteBtn.isChecked(),
                Integer.parseInt(mBinding.productCountTxt.getText().toString()));
    }

    // endregion


}
