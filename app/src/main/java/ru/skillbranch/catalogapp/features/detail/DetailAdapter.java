package ru.skillbranch.catalogapp.features.detail;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import mortar.MortarScope;
import ru.skillbranch.catalogapp.data.storage.ProductRealm;
import ru.skillbranch.catalogapp.di.DaggerService;
import ru.skillbranch.catalogapp.features.detail.comments.CommentScreen;
import ru.skillbranch.catalogapp.features.detail.decription.DescriptionScreen;
import ru.skillbranch.catalogapp.flow.AbstractScreen;

class DetailAdapter extends PagerAdapter {

    private ProductRealm mProductRealm;

    DetailAdapter(ProductRealm productRealm) {
        mProductRealm = productRealm;
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        AbstractScreen screen = null;

        switch (position) {
            case 0:
                screen = new DescriptionScreen(mProductRealm);
                break;
            case 1:
                screen = new CommentScreen(mProductRealm);
                break;
        }

        MortarScope screenScope = createScreenScopeFromContext(container.getContext(), screen);
        Context screenContext = screenScope.createContext(container.getContext());
        View view = LayoutInflater.from(screenContext).inflate(screen.getLayoutResId(), container, false);
        container.addView(view);
        return view;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        String title = "";
        switch (position) {
            case 0:
                title = "ОПИСАНИЕ";
                break;
            case 1:
                title = "ОТЗЫВЫ";
                break;
        }

        return title;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    private MortarScope createScreenScopeFromContext(Context context, AbstractScreen screen) {
        MortarScope parentScope = MortarScope.getScope(context);
        MortarScope childScope = parentScope.findChild(screen.getScopeName());
        if (childScope == null) {
            Object screenComponent = screen.createScreenComponent(parentScope.getService(DaggerService.SERVICE_NAME));
            if (screenComponent == null) {
                throw new IllegalStateException("Блин, опять забыл анатацию @Screen для " + screen.getScopeName());
            }
            childScope = parentScope.buildChild()
                    .withService(DaggerService.SERVICE_NAME, screenComponent)
                    .build(screen.getScopeName());
        }
        return childScope;
    }
}
