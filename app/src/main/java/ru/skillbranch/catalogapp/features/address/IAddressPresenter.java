package ru.skillbranch.catalogapp.features.address;

import ru.skillbranch.catalogapp.data.dto.AddressDto;

public interface IAddressPresenter {
    void updateOrInsertAddress(AddressDto address);
}
