package ru.skillbranch.catalogapp.features.catalog;

import android.os.Bundle;

import mortar.MortarScope;
import ru.skillbranch.catalogapp.data.storage.ProductRealm;
import ru.skillbranch.catalogapp.di.DaggerService;
import ru.skillbranch.catalogapp.features.auth.AuthView;
import ru.skillbranch.catalogapp.mvp.AbstractPresenter;
import rx.Subscriber;
import rx.Subscription;

class CatalogPresenter
        extends AbstractPresenter<CatalogView, CatalogModel>
        implements ICatalogPresenter {

    private int mRootProductCount = 0;
    private int mLastPagerPosition;

    CatalogPresenter() {
    }

    @Override
    protected void onLoad(Bundle savedInstanceState) {
        super.onLoad(savedInstanceState);
        mCompSubs.add(subscribeOnProductRealmObs());
    }

    private Subscription subscribeOnProductRealmObs() {
        if (getRootView() != null ) {
            getRootView().showLoad();
        }
        return mModel.getProductObs()
                .subscribe(new RealmSubscriber());
    }

    @Override
    public void dropView(CatalogView view) {
        mLastPagerPosition = getView().getCurrentPagerPosition();
        super.dropView(view);
    }

    @Override
    protected void initDagger(MortarScope scope) {
        ((CatalogScreen.CatalogComponent) scope.getService(DaggerService.SERVICE_NAME)).inject(this);
    }

    @Override
    protected void initActionBar() {
        mRootPresenter.newActionBarBuilder()
                .setTitle("")
                .setBackArrow(false)
                .setProductCardVisible(true)
                .build();
        mRootPresenter.newFabBuilder()
                .setFabVisible(false)
                .build();
    }

    // region ==================== ICatalogPresenter ====================

    @Override
    public void clickOnByBtn(int position) {
        if (getView() != null && getRootView() != null) {
            if (checkUserAuth()) {
                addProductToCard(position);
                getRootView().updateProductCounter(mRootProductCount);
                getRootView().showMessage("Товар " +
                        getView().getAdapter().getProductByPos(position).getProductName() +
                        "успешно добавлен в корзину");
            } else {
                getRootView().showAuthScreen(AuthView.LOGIN_STATE);
            }
        }
    }

    @Override
    public boolean checkUserAuth() {
        return mModel.checkUserAuth();
    }

    @Override
    public void addProductToCard(int position) {
        if (getView() != null) {
            mRootProductCount += getView().getAdapter().getProductByPos(position).getCount();
        }
    }

    // endregion

    private class RealmSubscriber extends Subscriber<ProductRealm> {
        CatalogAdapter mAdapter = getView().getAdapter();

        @Override
        public void onCompleted() {

        }

        @Override
        public void onError(Throwable e) {
            if (getRootView() != null) {
                getRootView().showError(e);
            }
        }

        @Override
        public void onNext(ProductRealm productRealm) {
            mAdapter.addItem(productRealm);
            if (getView() != null) {
                getView().updatePagerIndicator();
            }
            if (mAdapter.getCount() - 1 == mLastPagerPosition) {
                if (getRootView() != null) {
                    getRootView().hideLoad();
                }
                if (getView() != null) {
                    getView().showCatalogView();
                }
            }
        }
    }
}