package ru.skillbranch.catalogapp.features.auth;

import dagger.Provides;
import ru.skillbranch.catalogapp.R;
import ru.skillbranch.catalogapp.di.DaggerScope;
import ru.skillbranch.catalogapp.features.root.RootActivity;
import ru.skillbranch.catalogapp.flow.AbstractScreen;
import ru.skillbranch.catalogapp.flow.Screen;

@Screen(R.layout.screen_auth)
public class AuthScreen extends AbstractScreen<RootActivity.RootComponent> {
    private int mCustomState;

    public AuthScreen() {
        mCustomState = AuthView.IDLE_STATE;
    }

    public AuthScreen(int customState) {
        mCustomState = customState;
    }

    @Override
    public Object createScreenComponent(RootActivity.RootComponent parentComponent) {
        return DaggerAuthScreen_AuthComponent.builder()
                .rootComponent(parentComponent)
                .authModule(new AuthModule())
                .build();
    }

    // region ==================== Getters ====================

    int getCustomState() {
        return mCustomState;
    }

    // endregion

    // region ==================== Setters ====================

    void setCustomState(int customState) {
        mCustomState = customState;
    }

    // endregion

    // region ==================== DI ====================

    @dagger.Module
    class AuthModule {
        @DaggerScope(AuthScreen.class)
        @Provides
        AuthPresenter provideAuthPresenter () {
            return new AuthPresenter();
        }

        @DaggerScope(AuthScreen.class)
        @Provides
        AuthModel provideAuthModel () {
            return new AuthModel();
        }
    }

    @DaggerScope(AuthScreen.class)
    @dagger.Component(dependencies = RootActivity.RootComponent.class, modules = AuthModule.class)
    interface AuthComponent {
        void inject(AuthPresenter presenter);

        void inject(AuthView view);
    }

    // endregion
}
