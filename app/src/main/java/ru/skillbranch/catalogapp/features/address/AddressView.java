package ru.skillbranch.catalogapp.features.address;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.util.AttributeSet;

import flow.Flow;
import ru.skillbranch.catalogapp.R;
import ru.skillbranch.catalogapp.data.dto.AddressDto;
import ru.skillbranch.catalogapp.databinding.ScreenAddressBinding;
import ru.skillbranch.catalogapp.di.DaggerService;
import ru.skillbranch.catalogapp.mvp.AbstractView;

public class AddressView extends AbstractView<AddressPresenter> implements IAddressView {

    private ScreenAddressBinding mBinding;
    private AddressScreen mScreen;

    public AddressView(Context context, AttributeSet attrs) {
        super(context, attrs);
        if (!isInEditMode()){
            mScreen = Flow.getKey(context);
        }
    }

    // region ==================== AbstractView ====================

    @Override
    protected void initDagger(Context context) {
        DaggerService.<AddressScreen.AddressComponent>getDaggerComponent(context).inject(this);
    }

    @Override
    protected void initBinding() {
        mBinding = DataBindingUtil.bind(this);
    }

    @Override
    protected void attachListeners() {
        mBinding.addBtn.setOnClickListener(v -> mPresenter.updateOrInsertAddress(mScreen.getAddress()));
    }

    @Override
    protected void detachListeners() {
        mScreen = null;
        mBinding.addBtn.setOnClickListener(null);
        mBinding.unbind();
    }

    // endregion

    // region ==================== IAddressView ====================

    @Override
    public void showInputError() {

    }

    @Override
    public void initView() {
        AddressDto address = mScreen.getAddress();
        mBinding.setAddress(address);
        if (address.getId().isEmpty()) {
            mBinding.addBtn.setText(R.string.save_address);
        }
    }

    @Override
    public AddressDto getAddress() {
        return mScreen.getAddress();
    }

    @Override
    public boolean viewOnBackPressed() {
        return false;
    }

    // endregion
}