package ru.skillbranch.catalogapp.features.account;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;

import com.squareup.picasso.Picasso;

import javax.inject.Inject;

import flow.Flow;
import ru.skillbranch.catalogapp.R;
import ru.skillbranch.catalogapp.data.dto.SettingsDto;
import ru.skillbranch.catalogapp.data.dto.UserDto;
import ru.skillbranch.catalogapp.databinding.ScreenAccountBinding;
import ru.skillbranch.catalogapp.di.DaggerService;
import ru.skillbranch.catalogapp.mvp.AbstractView;
import ru.skillbranch.catalogapp.ui.helpers.ItemTouchHelperCallback;
import ru.skillbranch.catalogapp.ui.validators.PhoneValidator;
import ru.skillbranch.catalogapp.utils.TransformRounded;

public class AccountView extends AbstractView<AccountPresenter> implements IAccountView {

    public static final int PREVIEW_STATE = 0;
    public static final int EDIT_STATE = 1;

    private AccountScreen mScreen;
    private ScreenAccountBinding mBinding;
    private PhoneValidator mPhoneValidator;
    private UserDto mUser;
    private TextWatcher mNameWatcher;
    private ItemTouchHelper mItemTouchHelper;
    private AccountAdapter mAdapter;
    @Inject
    Picasso mPicasso;

    public AccountView(Context context, AttributeSet attrs) {
        super(context, attrs);
        if (!isInEditMode()) {
            mScreen = Flow.getKey(this);
        }
    }

    // region ==================== AbstractView ====================

    @Override
    protected void initDagger(Context context) {
        DaggerService.<AccountScreen.AccountComponent>getDaggerComponent(context).inject(this);
    }

    @Override
    protected void initBinding() {
        mBinding = DataBindingUtil.bind(this);
    }

    @Override
    protected void attachListeners() {
        mPhoneValidator = new PhoneValidator(getContext(),
                mBinding.accountInfo.userPhoneEt,
                mBinding.accountInfo.userPhoneWrap,
                R.string.phone_error_value);
        mBinding.accountInfo.userPhoneEt.addTextChangedListener(mPhoneValidator);

        mBinding.accountInfo.addBtn.setOnClickListener(v -> mPresenter.clickOnAddAddress());
        mBinding.accountHeader.setOnClickListener(v -> changeState());
    }

    @Override
    protected void detachListeners() {
        mBinding.accountInfo.userPhoneEt.removeTextChangedListener(mPhoneValidator);
        mBinding.accountInfo.addressList.swapAdapter(null, false);
        mBinding.accountInfo.addBtn.setOnClickListener(null);
        mBinding.accountHeader.setOnClickListener(null);

        if (mScreen.getCustomState() == EDIT_STATE) {
            mBinding.accountInfo.userNameEt.removeTextChangedListener(mNameWatcher);
        }
        mBinding.accountInfo.pushOrderSwt.setOnCheckedChangeListener(null);
        mBinding.accountInfo.pushPromoSwt.setOnCheckedChangeListener(null);
    }

    // endregion

    // region ==================== IAccountView ====================

    @Override
    public void initView(UserDto user) {
        mUser = user;
        initProfileInfo();
        initList();
        initState();
    }

    @Override
    public void initSettings(SettingsDto settings) {
        mBinding.accountInfo.setSetting(settings);
        mBinding.accountInfo.pushOrderSwt.setOnCheckedChangeListener((buttonView, isChecked) -> mPresenter.switchOrder(isChecked));
        mBinding.accountInfo.pushPromoSwt.setOnCheckedChangeListener((buttonView, isChecked) -> mPresenter.switchPromo(isChecked));
    }

    private void initProfileInfo() {
        mBinding.setUser(mUser);
        mBinding.accountInfo.setUser(mUser);
    }

    private void initList() {
        LinearLayoutManager linearLayoutManager =
                new LinearLayoutManager(mBinding.accountInfo.addressList.getContext());
        mBinding.accountInfo.addressList.setLayoutManager(linearLayoutManager);
        mBinding.accountInfo.addressList.setNestedScrollingEnabled(false);

        mAdapter = new AccountAdapter();
        mBinding.accountInfo.addressList.swapAdapter(mAdapter, false);

        ItemTouchHelper.Callback callback = new ItemTouchHelperCallback(getContext()) {
            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
                int position = viewHolder.getAdapterPosition();
                if (direction == 16) {
                    showRemoveAddressDialog(position);
                } else if (direction == 32){
                    showEditAddressDialog(position);
                }
            }
        };
        mItemTouchHelper = new ItemTouchHelper(callback);
        mItemTouchHelper.attachToRecyclerView(mBinding.accountInfo.addressList);
    }

    private void showRemoveAddressDialog(int position) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getContext());
        alertDialog.setTitle(R.string.remove_address_title);
        alertDialog.setMessage(R.string.remove_address);
        alertDialog.setPositiveButton(R.string.yes_caption, (dialog, arg1) ->
                mPresenter.removeAddress(mAdapter.getAddress(position).getId()));
        alertDialog.setNegativeButton(R.string.no_caption, (dialog, arg1) -> dialog.cancel());
        alertDialog.setCancelable(true);
        alertDialog.show();
    }

    private void showEditAddressDialog(int position) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getContext());
        alertDialog.setTitle(R.string.edit_address_title);
        alertDialog.setMessage(R.string.edit_address);
        alertDialog.setPositiveButton(R.string.yes_caption, (dialog, arg1) ->
                mPresenter.editAddress(mAdapter.getAddress(position)));
        alertDialog.setNegativeButton(R.string.no_caption, (dialog, arg1) -> dialog.cancel());
        alertDialog.setCancelable(true);
        alertDialog.show();
    }

    public AccountAdapter getAdapter() {
        return mAdapter;
    }

    private void initState() {
        if (mScreen.getCustomState() == PREVIEW_STATE) {
            showPreviewState();
        } else {
            showEditState();
        }
    }

    @Override
    public void changeState() {
        if (mScreen.getCustomState() == PREVIEW_STATE) {
            mScreen.setCustomState(EDIT_STATE);
        } else {
            mPresenter.saveUserProfileInfo();
            mScreen.setCustomState(PREVIEW_STATE);
        }
        initState();
    }

    @Override
    public void showEditState() {
         mNameWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                mBinding.userName.setText(mBinding.accountInfo.userNameEt.getText());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        };
        mBinding.accountInfo.userNameWrap.setVisibility(VISIBLE);
        mBinding.accountInfo.userNameEt.setFocusable(true);
        mBinding.accountInfo.userNameEt.setFocusableInTouchMode(true);

        mBinding.accountInfo.userPhoneEt.setFocusable(true);
        mBinding.accountInfo.userPhoneEt.setFocusableInTouchMode(true);
        mBinding.accountInfo.userNameEt.addTextChangedListener(mNameWatcher);
        mBinding.accountInfo.userPhoneEt.setEnabled(true);

        mBinding.accountImg.setOnClickListener(v -> {
            if (mScreen.getCustomState() == EDIT_STATE)
                showPhotoSourceDialog();
        });

        mPicasso.load(R.drawable.ic_add_circle_135dp)
                .into(mBinding.accountImg);
    }

    @Override
    public void showPreviewState() {
        mBinding.accountInfo.userNameWrap.setVisibility(GONE);
        mBinding.accountInfo.userNameEt.setFocusable(false);
        mBinding.accountInfo.userNameEt.setFocusableInTouchMode(false);

        mBinding.accountInfo.userNameEt.removeTextChangedListener(mNameWatcher);
        mBinding.accountInfo.userPhoneEt.setFocusable(false);
        mBinding.accountInfo.userPhoneEt.setFocusableInTouchMode(false);
        mBinding.accountInfo.userPhoneEt.setEnabled(false);
        mBinding.accountImg.setOnClickListener(null);
        updateAvatarPhoto();
    }

    @Override
    public void showPhotoSourceDialog() {
        String source[] = {"Загрузить из галлереи", "Сделать фото", "Отмена"};
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getContext());
        alertDialog.setTitle("Установить фото");
        alertDialog.setItems(source, (dialog, which) -> {
            switch (which) {
                case 0:
                    mPresenter.chooseGallery();
                    break;
                case 1:
                    mPresenter.chooseCamera();
                    break;
                case 2:
                    dialog.cancel();
                    break;
            }
        });
        alertDialog.show();
    }

    @Override
    public void updateAvatarPhoto() {
        if (mScreen.getCustomState() == PREVIEW_STATE) {
            mPicasso.load(Uri.parse(mUser.getAvatar()))
                .placeholder(R.drawable.ic_account_circle_135dp)
                .transform(new TransformRounded())
                .resize(136, 136)
                .centerCrop()
                .into(mBinding.accountImg);
        }
    }

    @Override
    public boolean viewOnBackPressed() {
        if (mScreen.getCustomState() == EDIT_STATE) {
            changeState();
            return true;
        } else {
            return false;
        }
    }

    // endregion
}