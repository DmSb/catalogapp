package ru.skillbranch.catalogapp.features.auth;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.design.widget.TextInputLayout;
import android.util.AttributeSet;

import flow.Flow;
import ru.skillbranch.catalogapp.R;
import ru.skillbranch.catalogapp.databinding.ScreenAuthBinding;
import ru.skillbranch.catalogapp.di.DaggerService;
import ru.skillbranch.catalogapp.mvp.AbstractView;
import ru.skillbranch.catalogapp.ui.validators.EmailValidator;
import ru.skillbranch.catalogapp.ui.validators.PasswordValidator;

public class AuthView extends AbstractView<AuthPresenter> implements IAuthView  {

    public static final int LOGIN_STATE = 0;
    public static final int IDLE_STATE = 1;

    private AuthScreen mScreen;
    private ScreenAuthBinding mBinding;
    private EmailValidator mEmailValidator;
    private PasswordValidator mPasswordValidator;

    public AuthView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    // region ==================== AbstractView ====================

    @Override
    protected void initDagger(Context context) {
        mScreen = Flow.getKey(this);
        DaggerService.<AuthScreen.AuthComponent>getDaggerComponent(context).inject(this);
    }

    @Override
    protected void initBinding() {
        mBinding = DataBindingUtil.bind(this);
        showViewFromState();
    }

    @Override
    protected void attachListeners() {
        mBinding.loginBtn.setOnClickListener(v -> mPresenter.clickOnLogin());
        mBinding.showCatalogBtn.setOnClickListener(v -> mPresenter.clickOnShowCatalog());
        mBinding.socialFbBtn.setOnClickListener(v -> mPresenter.clickOnFb());
        mBinding.socialTwitterBtn.setOnClickListener(v -> mPresenter.clickOnTwitter());
        mBinding.socialVkBtn.setOnClickListener(v -> mPresenter.clickOnVk());

        mEmailValidator = new EmailValidator(getContext(),
                mBinding.loginEmailEt,
                (TextInputLayout) findViewById(R.id.login_email_wrap),
                R.string.email_error_value);
        mBinding.loginEmailEt.addTextChangedListener(mEmailValidator);

        mPasswordValidator = new PasswordValidator(getContext(),
                mBinding.loginPasswordEt,
                (TextInputLayout) findViewById(R.id.login_password_wrap),
                R.string.password_error_value);
        mBinding.loginPasswordEt.addTextChangedListener(mPasswordValidator);
    }

    @Override
    protected void detachListeners() {
        mBinding.loginEmailEt.removeTextChangedListener(mEmailValidator);
        mEmailValidator = null;
        mBinding.loginEmailEt.setOnFocusChangeListener(null);
        mBinding.loginPasswordEt.removeTextChangedListener(mPasswordValidator);
        mPasswordValidator = null;
        mBinding.loginPasswordEt.setOnFocusChangeListener(null);

        mBinding.loginBtn.setOnClickListener(null);
        mBinding.showCatalogBtn.setOnClickListener(null);
        mBinding.socialFbBtn.setOnClickListener(null);
        mBinding.socialTwitterBtn.setOnClickListener(null);
        mBinding.socialVkBtn.setOnClickListener(null);
    }

    // endregion

    // region ==================== IAuthView, IView ====================

    @Override
    public void showLoginBtn() {
        mBinding.loginBtn.setVisibility(VISIBLE);
    }

    @Override
    public void hideLoginBtn() {
        mBinding.loginBtn.setVisibility(GONE);
    }

    @Override
    public String getUserEmail() {
        if (mEmailValidator.isValid()) {
            return String.valueOf(mBinding.loginEmailEt.getText());
        } else {
            return "";
        }
    }

    @Override
    public String getUserPassword() {
        if (mPasswordValidator.isValid()) {
            return String.valueOf(mBinding.loginPasswordEt.getText());
        } else {
            return "";
        }
    }

    @Override
    public void setCustomState(int state) {
        mScreen.setCustomState(state);
        showViewFromState();
    }

    @Override
    public void showLoginState() {
        mBinding.authCard.setVisibility(VISIBLE);
        mBinding.showCatalogBtn.setVisibility(GONE);
    }

    @Override
    public void showIdleState() {
        mBinding.authCard.setVisibility(GONE);
        mBinding.showCatalogBtn.setVisibility(VISIBLE);
    }

    @Override
    public void showViewFromState() {
        if (mScreen.getCustomState() == LOGIN_STATE) {
            showLoginState();
        } else {
            showIdleState();
        }
    }

    @Override
    public boolean isIdle() {
        return mScreen.getCustomState() == IDLE_STATE;
    }

    @Override
    public boolean viewOnBackPressed() {
        if (mScreen.getCustomState() == LOGIN_STATE) {
            setCustomState(IDLE_STATE);
            return true;
        } else {
            return false;
        }
    }

    // endregion
}