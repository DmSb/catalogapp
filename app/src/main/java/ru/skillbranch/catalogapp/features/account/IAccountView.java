package ru.skillbranch.catalogapp.features.account;

import android.net.Uri;

import ru.skillbranch.catalogapp.data.dto.SettingsDto;
import ru.skillbranch.catalogapp.data.dto.UserDto;
import ru.skillbranch.catalogapp.mvp.IView;

public interface IAccountView extends IView {
    void initView(UserDto user);
    void initSettings(SettingsDto settings);

    void changeState();
    void showEditState();
    void showPreviewState();
    void showPhotoSourceDialog();
    void updateAvatarPhoto();
}
