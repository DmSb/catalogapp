package ru.skillbranch.catalogapp.features.detail;

import android.support.v4.view.ViewPager;

import ru.skillbranch.catalogapp.data.storage.ProductRealm;
import ru.skillbranch.catalogapp.mvp.IView;

interface IDetailView extends IView {

    void initView(ProductRealm product);
    ViewPager getViewPager();
}
