package ru.skillbranch.catalogapp.features.auth;

import ru.skillbranch.catalogapp.mvp.IView;

public interface IAuthView extends IView {

    void showLoginBtn();
    void hideLoginBtn();

    String getUserEmail();
    String getUserPassword();

    boolean isIdle();

    void setCustomState(int state);
    void showLoginState();
    void showIdleState();
    void showViewFromState();
}
