package ru.skillbranch.catalogapp.features.account;

import java.util.ArrayList;

import ru.skillbranch.catalogapp.data.dto.AddressDto;
import ru.skillbranch.catalogapp.data.dto.SettingsDto;
import ru.skillbranch.catalogapp.data.dto.UserDto;
import ru.skillbranch.catalogapp.mvp.AbstractModel;
import rx.Observable;
import rx.subjects.BehaviorSubject;

public class AccountModel extends AbstractModel {

    private BehaviorSubject<UserDto> mUserInfoObs = BehaviorSubject.create();

    public AccountModel() {
        mUserInfoObs.onNext(getUser());
    }

    // region ==================== UserInfo ====================

    public UserDto getUser(){
        return mDataManager.getUser();
    }

    void saveUserInfo() {
        mDataManager.saveUserProfileInfo();
        mUserInfoObs.onNext(getUser());
    }

    public Observable<UserDto> getUserInfoObs() {
        return mUserInfoObs;
    }

    // endregion

    // region ==================== Addresses ====================

    Observable<AddressDto> getAddressObs() {
        return Observable.from(getAddressList());
    }

    private ArrayList<AddressDto> getAddressList() {
        return mDataManager.getAddressList();
    }

    // endregion

    // region ==================== Settings ====================

    Observable<SettingsDto> getSetiingObs() {
        return Observable.just(getSettings());
    }

    private SettingsDto getSettings() {
        return mDataManager.getSetting();
    }

    void setOrderNotification(boolean isChecked) {
        mDataManager.setOrderNotification(isChecked);
    }

    void setPromoNotification(boolean isChecked) {
        mDataManager.setPromoNotification(isChecked);
    }

    // endregion
}