package ru.skillbranch.catalogapp.features.account;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import ru.skillbranch.catalogapp.R;
import ru.skillbranch.catalogapp.data.dto.AddressDto;
import ru.skillbranch.catalogapp.databinding.AddressItemBinding;

class AccountAdapter extends RecyclerView.Adapter<AccountAdapter.AddressViewHolder> {

    private ArrayList<AddressDto> mAddressList;

    AccountAdapter() {
        mAddressList = new ArrayList<>();
    }

    void addItem(AddressDto address) {
        mAddressList.add(address);
        notifyDataSetChanged();
    }

    void reloadAdapter() {
        mAddressList.clear();
        notifyDataSetChanged();
    }

    @Override
    public AddressViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.address_item, parent, false);
        return new AddressViewHolder(view);
    }

    @Override
    public void onBindViewHolder(AddressViewHolder holder, int position) {
        holder.mBinding.setAddress(mAddressList.get(position));
    }

    @Override
    public int getItemCount() {
        return mAddressList.size();
    }

    AddressDto getAddress(int position) {
        return mAddressList.get(position);
    }

    // region ==================== AddressViewHolder ====================

    static class AddressViewHolder extends RecyclerView.ViewHolder {

        AddressItemBinding mBinding;

        AddressViewHolder(View itemView) {
            super(itemView);
            mBinding = DataBindingUtil.bind(itemView);
        }
    }

    // endregion
}