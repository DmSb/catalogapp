package ru.skillbranch.catalogapp.features.detail;

import ru.skillbranch.catalogapp.mvp.AbstractModel;

public class DetailModel extends AbstractModel {
    public String getUserName() {
        return mDataManager.getUser().getFullName();
    }

    public String getAvatar() {
        return mDataManager.getUser().getAvatar();
    }
}
