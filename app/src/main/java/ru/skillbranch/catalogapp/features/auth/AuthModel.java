package ru.skillbranch.catalogapp.features.auth;


import ru.skillbranch.catalogapp.mvp.AbstractModel;

class AuthModel extends AbstractModel {

    boolean isUserAuth() {
        return mDataManager.isAuthUser();
    }

    void loginUser(String email, String password) {
        if (!email.isEmpty() && !password.isEmpty()) {
            mDataManager.saveToken(email + " " + password);
        }
    }
}