package ru.skillbranch.catalogapp.features.catalog;

public interface ICatalogPresenter {
    void clickOnByBtn(int position);
    boolean checkUserAuth();
    void addProductToCard(int position);
}
