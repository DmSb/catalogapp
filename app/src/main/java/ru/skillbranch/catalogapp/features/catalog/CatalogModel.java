package ru.skillbranch.catalogapp.features.catalog;

import com.fernandocejas.frodo.annotation.RxLogObservable;

import ru.skillbranch.catalogapp.data.storage.ProductRealm;
import ru.skillbranch.catalogapp.mvp.AbstractModel;
import rx.Observable;

public class CatalogModel extends AbstractModel {

    CatalogModel() {
    }

    boolean checkUserAuth() {
        return mDataManager.isAuthUser();
    }

    Observable<ProductRealm> getProductObs() {
        Observable<ProductRealm> diskObs = getProductObsFromDisk();
        Observable<ProductRealm> networkObs = getProductObsFromNetwork();

        return Observable.mergeDelayError(diskObs, networkObs)
                .distinct(ProductRealm::getId);
    }

    @RxLogObservable
    private Observable<ProductRealm> getProductObsFromNetwork() {
        return  mDataManager.getProductObsFromNetwork();
    }

    @RxLogObservable
    private Observable<ProductRealm> getProductObsFromDisk() {
        return mDataManager.getProductFromRealm();
    }
}