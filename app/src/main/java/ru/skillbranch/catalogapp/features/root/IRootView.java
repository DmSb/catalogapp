package ru.skillbranch.catalogapp.features.root;

import android.support.annotation.Nullable;

import ru.skillbranch.catalogapp.mvp.IView;

public interface IRootView {
    void showMessage(String message);
    void showError(Throwable e);
    void showPermissionError();
    void showLoad();
    void hideLoad();

    void loadUserAvatar(String userAvatar);
    void setUseName(String userName);
    void updateProductCounter(int productCount);


    @Nullable
    IView getCurrentScreen();

    void showCatalogScreen();
    void showAccountScreen();
    void showAuthScreen(int customState);
}
