package ru.skillbranch.catalogapp.features.account;


import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;

import com.squareup.leakcanary.RefWatcher;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.inject.Inject;

import flow.Flow;
import io.realm.RealmResults;
import mortar.MortarScope;
import ru.skillbranch.catalogapp.R;
import ru.skillbranch.catalogapp.data.dto.ActivityResultDto;
import ru.skillbranch.catalogapp.data.dto.AddressDto;
import ru.skillbranch.catalogapp.data.dto.PermissionResultDto;
import ru.skillbranch.catalogapp.data.dto.SettingsDto;
import ru.skillbranch.catalogapp.data.dto.UserDto;
import ru.skillbranch.catalogapp.data.storage.AddressRealm;
import ru.skillbranch.catalogapp.data.storage.RealmTransactionExecutor;
import ru.skillbranch.catalogapp.di.DaggerService;
import ru.skillbranch.catalogapp.features.address.AddressScreen;
import ru.skillbranch.catalogapp.features.root.RootActivity;
import ru.skillbranch.catalogapp.mvp.AbstractPresenter;
import ru.skillbranch.catalogapp.utils.ConstantManager;
import ru.skillbranch.catalogapp.utils.Lg;
import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

import static android.Manifest.permission.CAMERA;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;
import static android.content.pm.PackageManager.PERMISSION_DENIED;

class AccountPresenter extends AbstractPresenter<AccountView, AccountModel>
        implements IAccountPresenter {

    @Inject
    RefWatcher mRefWatcher;

    private Subscription mActivityResultSub, mPermissionSub;
    private File mPhotoFile;

    @Override
    protected void initDagger(MortarScope scope) {
        ((AccountScreen.AccountComponent) scope.getService(DaggerService.SERVICE_NAME)).inject(this);
    }

    @Override
    protected void initActionBar() {
        Lg.d("initActionBar");
        mRootPresenter.newActionBarBuilder()
                .setTitle(getView().getContext().getResources().getString(R.string.profile_info))
                .setBackArrow(false)
                .setProductCardVisible(false)
                .build();
    }

    // region ==================== LifeCycle ====================

    @Override
    protected void onLoad(Bundle savedInstanceState) {
        super.onLoad(savedInstanceState);

        if (getView() != null && getRootView() != null) {
            mCompSubs.add(subscribeOnAddressObs());
            mCompSubs.add(subscribeOnSettingObs());
            mCompSubs.add(subscribeOnUserInfoObs());

            getView().initView(mModel.getUser());
        }
    }

    @Override
    protected void onEnterScope(MortarScope scope) {
        super.onEnterScope(scope);

        subscribeOnActivityResult();
    }

    @Override
    protected void onSave(Bundle outState) {
        super.onSave(outState);
        mCompSubs.unsubscribe();
    }

    @Override
    protected void onExitScope() {
        super.onExitScope();
        mActivityResultSub.unsubscribe();
        mRefWatcher.watch(this);
    }

    // endregion

    // region ==================== IAccountPresenter ====================

    @Override
    public void switchOrder(boolean isChecked) {
        mModel.setOrderNotification(isChecked);
    }

    @Override
    public void switchPromo(boolean isChecked) {
        mModel.setPromoNotification(isChecked);
    }

    @Override
    public void clickOnAddAddress() {
        if (getView() != null) {
            Flow.get(getView()).set(new AddressScreen(null));
        }
    }

    @Override
    public void removeAddress(String addressId) {
        RealmTransactionExecutor.executeRealmTransaction(realm -> {
                RealmResults<AddressRealm> addressResults = realm
                        .where(AddressRealm.class)
                        .equalTo("id", addressId)
                        .findAll();
                 addressResults.deleteAllFromRealm();
        });
        updateListView();
    }

    @Override
    public void editAddress(AddressDto address) {
        if (getView() != null) {
            Flow.get(getView()).set(new AddressScreen(address));
        }
    }

    @Override
    public void saveUserProfileInfo() {
        mModel.saveUserInfo();
    }

    @Override
    public void takePhoto() {
        if (getView() != null) {
            getView().showPhotoSourceDialog();
        }
    }

    // endregion

    // region ==================== Camera ====================

    @Override
    public void chooseCamera() {
        if (getRootView() != null) {
            String[] permissions = new String[] {CAMERA, WRITE_EXTERNAL_STORAGE};
            if (mRootPresenter.checkPermissionsAndRequestIfNotGranted(permissions,
                    ConstantManager.REQUEST_PERMISSION_CAMERA)) {
                mPhotoFile = createFileForPhoto();
                if (mPhotoFile == null) {
                    getRootView().showMessage(ConstantManager.FILE_DONT_CREATE_MESSAGE);
                    return;
                }
                takePhotoFromCamera(mPhotoFile);
            }
        }
    }

    private void takePhotoFromCamera(File file) {
        Uri uri = Uri.fromFile(file);
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
        if (getRootView() != null) {
            ((RootActivity) getRootView()).startActivityForResult(intent,
                ConstantManager.REQUEST_PROFILE_PHOTO_CAMERA);
        }
    }

    /**
     * Get catalog to create photo file
     * @return - catalog
     */
    private File getExternalStoragePictureDirectory() {
        if (Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
            File storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
            if (storageDir.exists()) {
                return storageDir;
            } else {
                storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM);
                if (storageDir.exists()) {
                    return storageDir;
                } else {
                    storageDir = Environment.getExternalStorageDirectory();
                    if (storageDir.exists()) {
                        return storageDir;
                    } else {
                        return null;
                    }
                }
            }
        } else {
            return null;
        }
    }

    private File createFileForPhoto() {
        DateFormat dateTimeInstance = new SimpleDateFormat("yyyyMMdd_HHmmss");
        String timeStamp = dateTimeInstance.format(new Date());
        String imageFileName = "IMG_" + timeStamp;
        File storageDir = getExternalStoragePictureDirectory();
        File fileImage = null;
        try {
            fileImage = File.createTempFile(imageFileName, ".jpeg", storageDir);
        } catch (IOException e) {
            if (getRootView() != null)
                getRootView().showError(e);
        }
        return fileImage;
    }

    // endregion

    // region ==================== Gallery ====================

    @Override
    public void chooseGallery() {
        if (getRootView() != null) {
            String[] permissions = new String[] {WRITE_EXTERNAL_STORAGE};
            if (mRootPresenter.checkPermissionsAndRequestIfNotGranted(permissions,
                    ConstantManager.REQUEST_PERMISSION_READ_EXTERNAL_STORAGE)) {

                takePhotoFromGallery();
            }
        }
    }

    private void takePhotoFromGallery() {
        Intent intent = new Intent();
        intent.setType("image/*");
        if (Build.VERSION.SDK_INT < 19) {
            intent.setAction(Intent.ACTION_GET_CONTENT);
        } else {
            intent.setAction(Intent.ACTION_OPEN_DOCUMENT);
            intent.addCategory(Intent.CATEGORY_OPENABLE);
        }
        if (getRootView() != null) {
            ((RootActivity) getRootView()).startActivityForResult(intent, ConstantManager.REQUEST_PROFILE_PHOTO_PICKER);
        }
    }

    // endregion

    // region ==================== Subscription ====================

    private Subscription subscribeOnAddressObs() {
        return subscribe(mModel.getAddressObs(),
                new ViewSubscriber<AddressDto>() {
                    @Override
                    public void onNext(AddressDto address) {
                        if (getView() != null) {
                            getView().getAdapter().addItem(address);
                        }
                    }
                });
    }

    private void updateListView() {
        if (getView() != null) {
            getView().getAdapter().reloadAdapter();
            subscribeOnAddressObs();
        }
    }


    private Subscription subscribeOnSettingObs() {
        return subscribe(mModel.getSetiingObs(),
                new ViewSubscriber<SettingsDto>() {
                    @Override
                    public void onNext(SettingsDto settings) {
                        if (getView() != null) {
                            getView().initSettings(settings);
                        }
                    }
                });
    }

    private void subscribeOnActivityResult() {
        Observable<ActivityResultDto> activityResultObs = mRootPresenter.getActivityResultObs()
                .filter(activityResult -> activityResult.getResultCode() == Activity.RESULT_OK);
        
        mActivityResultSub = subscribe(activityResultObs, new ViewSubscriber<ActivityResultDto>() {
            @Override
            public void onNext(ActivityResultDto activityResult) {
                handleActivityResult(activityResult);
            }
        });
    }

    private Subscription subscribeOnUserInfoObs() {
        return subscribe(mModel.getUserInfoObs(), new ViewSubscriber<UserDto>() {
            @Override
            public void onNext(UserDto user) {
                if (getView() != null) {
                    getView().updateAvatarPhoto();
                }
            }
        });
    }

    private void subscribeOnPermissionResult() {
        if (mRootPresenter.getPermissionResultObs() != null) {
            mPermissionSub = subscribe(mRootPresenter.getPermissionResultObs(),
                    new ViewSubscriber<PermissionResultDto>() {
                @Override
                public void onNext(PermissionResultDto permissionResult) {
                    handlePermissionResult(permissionResult);     
                }
            });
        }
    }

    private void handleActivityResult(ActivityResultDto activityResult) {
        Observable.just(activityResult)
                .subscribeOn(Schedulers.computation())
                .map(activityResult1 -> {
                    switch (activityResult1.getRequestCode()) {
                        case ConstantManager.REQUEST_PROFILE_PHOTO_CAMERA:
                            return Uri.fromFile(mPhotoFile);
                        case ConstantManager.REQUEST_PROFILE_PHOTO_PICKER:
                            return activityResult1.getIntent().getData();
                        default:
                            return null;
                    }
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(uri -> {
                    updateAvatar(uri);
                });
    }

    private void updateAvatar(Uri uri) {
        if (uri != null) {
            mModel.getUser().setAvatar(uri.toString());
            mModel.saveUserInfo();
        }
    }

    private void handlePermissionResult(PermissionResultDto permissionResult) {
        Observable.just(permissionResult)
                .subscribeOn(Schedulers.computation())
                .filter(permissionResult1 -> permissionResult1.getGrantResults().length > 0)
                .filter(permissionResult12 -> {
                    for (int grantResult : permissionResult12.getGrantResults()) {
                        if (grantResult == PERMISSION_DENIED) {
                            showPermissionError();
                            return false;
                        }
                    }
                    return true;
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(permissionResult13 -> {
                    switch (permissionResult13.getRequestCode()) {
                        case ConstantManager.REQUEST_PERMISSION_CAMERA:
                            chooseCamera();
                            break;
                        case ConstantManager.REQUEST_PERMISSION_READ_EXTERNAL_STORAGE:
                            chooseGallery();
                            break;
                    }
                });
    }

    private void showPermissionError() {
        if (getRootView() != null) {
            getRootView().showPermissionError();
        }
    }

    // endregion
}