package ru.skillbranch.catalogapp.features.auth;

import android.os.Bundle;
import android.os.Handler;

import mortar.MortarScope;
import ru.skillbranch.catalogapp.di.DaggerService;
import ru.skillbranch.catalogapp.mvp.AbstractPresenter;

class AuthPresenter extends AbstractPresenter<AuthView, AuthModel> implements IAuthPresenter {

    @Override
    protected void onLoad(Bundle savedInstanceState) {
        super.onLoad(savedInstanceState);

        if (getView() != null && getRootView() != null) {
            if (checkUserAuth()) {
                getView().hideLoginBtn();
            } else {
                getView().showLoginBtn();
            }
        }
    }

    // region ==================== AbstractPresenter ====================

    @Override
    protected void initDagger(MortarScope scope) {
        ((AuthScreen.AuthComponent) scope.getService(DaggerService.SERVICE_NAME)).inject(this);
    }

    @Override
    protected void initActionBar() {
        mRootPresenter.newActionBarBuilder()
                .setTitle("")
                .setBackArrow(false)
                .build();
    }

    // endregion

    // region ==================== IAuthPresenter ====================

    @Override
    public void clickOnLogin() {
        if (getView() != null && getRootView() != null) {
            if (getView().isIdle()) {
                getView().setCustomState(AuthView.LOGIN_STATE);
            } else {
                String userEmail = getView().getUserEmail();
                String userPassword = getView().getUserPassword();

                if (!userEmail.isEmpty() && !userPassword.isEmpty()) {
                    getRootView().showLoad();
                    runWithDelay();
                    mModel.loginUser(userEmail, userPassword);
                }
            }
        }
    }

    @Override
    public void clickOnFb() {
        if (getView() != null && getRootView() != null)
            getRootView().showMessage("clickOnFb");
    }

    @Override
    public void clickOnVk() {
        if (getView() != null && getRootView() != null)
            getRootView().showMessage("clickOnVk");
    }

    @Override
    public void clickOnTwitter() {
        if (getView() != null && getRootView() != null)
            getRootView().showMessage("clickOnTwitter");
    }

    @Override
    public void clickOnShowCatalog() {
        if (getRootView() != null)
            getRootView().showCatalogScreen();
    }

    @Override
    public boolean checkUserAuth() {
        return mModel.isUserAuth();
    }

    // endregion

    private void runWithDelay(){
        final Handler handler = new Handler();
        handler.postDelayed(
                () -> {
                    if (getView() != null && getRootView() != null) {
                        getRootView().hideLoad();
                        if (mModel.isUserAuth()) {
                            clickOnShowCatalog();
                        } else {
                            getRootView().showMessage("Ошибка авторизации");
                        }
                    }
                }, 3000
        );
    }
}