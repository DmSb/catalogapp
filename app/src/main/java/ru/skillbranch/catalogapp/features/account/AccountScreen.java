package ru.skillbranch.catalogapp.features.account;

import com.squareup.picasso.Picasso;

import dagger.Provides;
import ru.skillbranch.catalogapp.R;
import ru.skillbranch.catalogapp.features.root.RootActivity;
import ru.skillbranch.catalogapp.features.root.RootPresenter;
import ru.skillbranch.catalogapp.flow.AbstractScreen;
import ru.skillbranch.catalogapp.flow.Screen;

@Screen(R.layout.screen_account)
public class AccountScreen extends AbstractScreen<RootActivity.RootComponent> {

    private int mCustomState = AccountView.PREVIEW_STATE;

    @Override
    public Object createScreenComponent(RootActivity.RootComponent parentComponent) {
        return DaggerAccountScreen_AccountComponent.builder()
                .rootComponent(parentComponent)
                .accountModule(new AccountModule())
                .build();
    }

    int getCustomState() {
        return mCustomState;
    }

    void setCustomState(int customState) {
        mCustomState = customState;
    }

    // region ==================== DI ====================

    @dagger.Module
    class AccountModule {
        @Provides
        @AccountScope
        AccountPresenter provideAccountPresenter () {
            return new AccountPresenter();
        }
    }

    @AccountScope
    @dagger.Component(dependencies = RootActivity.RootComponent.class, modules = AccountModule.class)
    public interface AccountComponent {
        void inject(AccountPresenter presenter);
        void inject(AccountView view);

        RootPresenter getRootPresenter();
        AccountModel getAccountModel();
        Picasso getPicasso();
    }

    // endregion
}
