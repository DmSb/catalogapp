package ru.skillbranch.catalogapp.features.address;

import ru.skillbranch.catalogapp.data.dto.AddressDto;
import ru.skillbranch.catalogapp.mvp.IView;

public interface IAddressView extends IView {
    void showInputError();
    void initView();
    AddressDto getAddress();
}
