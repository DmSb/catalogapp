package ru.skillbranch.catalogapp.features.detail.comments;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import ru.skillbranch.catalogapp.R;
import ru.skillbranch.catalogapp.data.dto.CommentDto;
import ru.skillbranch.catalogapp.databinding.CommentItemBinding;

class CommentAdapter extends RecyclerView.Adapter<CommentAdapter.CommentViewHolder> {
    private List<CommentDto> mCommentList = new ArrayList<>();

    @Override
    public CommentViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new CommentViewHolder(LayoutInflater
                .from(parent.getContext())
                .inflate(R.layout.comment_item, parent, false));
    }

    @Override
    public void onBindViewHolder(CommentViewHolder holder, int position) {
        CommentDto comment = mCommentList.get(position);
        holder.mBinding.setComment(comment);
    }

    @Override
    public int getItemCount() {
        return mCommentList.size();
    }

    void addComments(CommentDto commentDto) {
        mCommentList.add(commentDto);
        notifyDataSetChanged();
    }

    void reloadAdapter(List<CommentDto> commentDtos) {
        mCommentList.clear();
        mCommentList = commentDtos;
        notifyDataSetChanged();
    }

    class CommentViewHolder extends RecyclerView.ViewHolder {
        CommentItemBinding mBinding;

        CommentViewHolder(View itemView) {
            super(itemView);
            mBinding = DataBindingUtil.bind(itemView);
        }
    }
}
