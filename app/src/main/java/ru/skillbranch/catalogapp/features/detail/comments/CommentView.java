package ru.skillbranch.catalogapp.features.detail.comments;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;

import ru.skillbranch.catalogapp.R;
import ru.skillbranch.catalogapp.data.storage.CommentRealm;
import ru.skillbranch.catalogapp.databinding.DialogCommentBinding;
import ru.skillbranch.catalogapp.databinding.ScreenCommentBinding;
import ru.skillbranch.catalogapp.di.DaggerService;
import ru.skillbranch.catalogapp.mvp.AbstractView;
import ru.skillbranch.catalogapp.mvp.IInitFabView;

public class CommentView extends AbstractView<CommentPresenter> implements IInitFabView {

    private CommentAdapter mAdapter = new CommentAdapter();
    private ScreenCommentBinding mBinding;

    public CommentView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void initDagger(Context context) {
        DaggerService.<CommentScreen.CommentComponent>getDaggerComponent(context).inject(this);
    }

    @Override
    protected void initBinding() {
        mBinding = DataBindingUtil.bind(this);
    }

    @Override
    protected void attachListeners() {
        // empty
    }

    @Override
    protected void detachListeners() {
        // empty
    }

    public CommentAdapter getAdapter() {
        return mAdapter;
    }

    public void initView() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        mBinding.commentRecycler.setLayoutManager(layoutManager);
        mBinding.commentRecycler.setAdapter(mAdapter);
    }

    @Override
    public boolean viewOnBackPressed() {
        return false;
    }

    public void showAddCommentDialog() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getContext());
        LayoutInflater inflater = LayoutInflater.from(getContext());

        View dialogView = inflater.inflate(R.layout.dialog_comment, null);
        DialogCommentBinding binding = DataBindingUtil.bind(dialogView);

        dialogBuilder.setTitle("Оставить отзыв о товаре?")
                .setView(dialogView)
                .setPositiveButton("Оставить отзыв", (dialog, which) -> {
                    CommentRealm commentRealm = new CommentRealm(
                            mPresenter.getUserName(),
                            mPresenter.getAvatar(),
                            binding.commentRb.getRating(),
                            binding.commentEt.getText().toString());
                    mPresenter.addComment(commentRealm);
                })
                .setNegativeButton("Отмена", (dialog, which) -> dialog.cancel())
                .show();
    }

    @Override
    public void initFab() {
        mPresenter.initFab();
    }
}
