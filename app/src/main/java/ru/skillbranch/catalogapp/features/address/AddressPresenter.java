package ru.skillbranch.catalogapp.features.address;

import android.os.Bundle;

import flow.Flow;
import mortar.MortarScope;
import ru.skillbranch.catalogapp.R;
import ru.skillbranch.catalogapp.data.dto.AddressDto;
import ru.skillbranch.catalogapp.data.storage.AddressRealm;
import ru.skillbranch.catalogapp.data.storage.RealmTransactionExecutor;
import ru.skillbranch.catalogapp.di.DaggerService;
import ru.skillbranch.catalogapp.features.account.AccountModel;
import ru.skillbranch.catalogapp.mvp.AbstractPresenter;

class AddressPresenter extends AbstractPresenter<AddressView, AccountModel>
        implements IAddressPresenter {

    @Override
    protected void onLoad(Bundle savedInstanceState) {
        super.onLoad(savedInstanceState);
        if (getView() != null) {
            getView().initView();
        }
    }

    @Override
    protected void initDagger(MortarScope scope) {
        ((AddressScreen.AddressComponent) scope.getService(DaggerService.SERVICE_NAME)).inject(this);
    }

    @Override
    protected void initActionBar() {
        mRootPresenter.newActionBarBuilder()
                .setTitle(getView().getContext().getResources().getString(R.string.address_title))
                .setBackArrow(true)
                .setProductCardVisible(false)
                .build();
    }

    // region ==================== IAddressPresenter ====================

    @Override
    public void updateOrInsertAddress(AddressDto address) {
        RealmTransactionExecutor.executeRealmTransaction(realm -> {
            AddressRealm addressRealm = new AddressRealm(address);
            realm.insertOrUpdate(addressRealm);
        });

        Flow.get(getView()).goBack();
    }

    // endregion
}