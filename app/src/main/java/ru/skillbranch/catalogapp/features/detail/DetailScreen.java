package ru.skillbranch.catalogapp.features.detail;

import dagger.Provides;
import flow.TreeKey;
import ru.skillbranch.catalogapp.R;
import ru.skillbranch.catalogapp.data.storage.ProductRealm;
import ru.skillbranch.catalogapp.di.DaggerScope;
import ru.skillbranch.catalogapp.features.catalog.CatalogScreen;
import ru.skillbranch.catalogapp.features.product.ProductScreen;
import ru.skillbranch.catalogapp.features.root.RootPresenter;
import ru.skillbranch.catalogapp.flow.AbstractScreen;
import ru.skillbranch.catalogapp.flow.Screen;

@Screen(R.layout.screen_detail)
public class DetailScreen extends AbstractScreen<CatalogScreen.CatalogComponent>
        implements TreeKey {

    private final ProductRealm mProduct;

    public DetailScreen(ProductRealm product) {
        mProduct = product;
    }

    // region ==================== AbstractScreen ====================

    @Override
    public Object createScreenComponent(CatalogScreen.CatalogComponent parentComponent) {
        return DaggerDetailScreen_DetailComponent.builder()
                .catalogComponent(parentComponent)
                .detailModule(new DetailModule())
                .build();
    }

    @Override
    public Object getParentKey() {
        return new ProductScreen(mProduct);
    }

    // endregion

    // region ==================== DI ====================

    @dagger.Module
    class DetailModule {
        @Provides
        @DaggerScope(DetailScreen.class)
        DetailPresenter provideDetailPresenter () {
            return new DetailPresenter(mProduct);
        }

        @Provides
        @DaggerScope(DetailScreen.class)
        DetailModel provideDetailModel () {
            return new DetailModel();
        }
    }

    @dagger.Component(dependencies = CatalogScreen.CatalogComponent.class, modules = DetailModule.class)
    @DaggerScope(DetailScreen.class)
    public interface DetailComponent {
        void inject(DetailPresenter presenter);
        void inject(DetailView view);

        DetailModel getDetailModel();
        RootPresenter getRootPresenter();
    }

    // endregion
}
