package ru.skillbranch.catalogapp.features.catalog;

import android.content.Context;

import com.squareup.picasso.Picasso;

import dagger.Provides;
import mortar.MortarScope;
import ru.skillbranch.catalogapp.R;
import ru.skillbranch.catalogapp.data.storage.ProductRealm;
import ru.skillbranch.catalogapp.di.DaggerScope;
import ru.skillbranch.catalogapp.di.DaggerService;
import ru.skillbranch.catalogapp.features.product.ProductScreen;
import ru.skillbranch.catalogapp.features.root.RootActivity;
import ru.skillbranch.catalogapp.features.root.RootPresenter;
import ru.skillbranch.catalogapp.flow.AbstractScreen;
import ru.skillbranch.catalogapp.flow.Screen;

@Screen(R.layout.screen_catalog)
public class CatalogScreen extends AbstractScreen<RootActivity.RootComponent> {

    @Override
    public Object createScreenComponent(RootActivity.RootComponent parentComponent) {
        return DaggerCatalogScreen_CatalogComponent.builder()
                .rootComponent(parentComponent)
                .catalogModule(new CatalogModule())
                .build();
    }

    // region ==================== DI ====================

    @dagger.Module
    class CatalogModule {
        @DaggerScope(CatalogScreen.class)
        @Provides
        CatalogPresenter provideCatalogPresenter () {
            return new CatalogPresenter();
        }

        @DaggerScope(CatalogScreen.class)
        @Provides
        CatalogModel provideCatalogModel () {
            return new CatalogModel();
        }
    }

    @DaggerScope(CatalogScreen.class)
    @dagger.Component(dependencies = RootActivity.RootComponent.class,
            modules = CatalogScreen.CatalogModule.class)
    public interface CatalogComponent {
        void inject(CatalogPresenter presenter);

        void inject(CatalogView view);

        CatalogModel getCatalogModel();
        Picasso getPicasso();
        RootPresenter getRootPresenter();
    }

    // endregion

    public static class Factory {
        static Context createProductContext(ProductRealm product, Context parentContext) {
            MortarScope parentScope = MortarScope.getScope(parentContext);
            MortarScope childScope;
            ProductScreen screen = new ProductScreen(product);
            String scopeName = String.format("%s_%s", screen.getScopeName(), product.getId());

            if (parentScope.findChild(scopeName) == null) {
                childScope = parentScope.buildChild()
                        .withService(DaggerService.SERVICE_NAME,
                                screen.createScreenComponent(DaggerService.<CatalogScreen.CatalogComponent>getDaggerComponent(parentContext)))
                        .build(scopeName);
            } else {
                childScope = parentScope.findChild(scopeName);
            }
            return childScope.createContext(parentContext);
        }
    }
}