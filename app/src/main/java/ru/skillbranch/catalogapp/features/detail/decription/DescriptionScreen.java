package ru.skillbranch.catalogapp.features.detail.decription;

import dagger.Provides;
import ru.skillbranch.catalogapp.R;
import ru.skillbranch.catalogapp.data.storage.ProductRealm;
import ru.skillbranch.catalogapp.di.DaggerScope;
import ru.skillbranch.catalogapp.features.detail.DetailScreen;
import ru.skillbranch.catalogapp.flow.AbstractScreen;
import ru.skillbranch.catalogapp.flow.Screen;

@Screen(R.layout.screen_description)
public class DescriptionScreen extends AbstractScreen<DetailScreen.DetailComponent> {
    private ProductRealm mProductRealm;

    public DescriptionScreen(ProductRealm productRealm) {
        mProductRealm = productRealm;
    }

    @Override
    public Object createScreenComponent(DetailScreen.DetailComponent parentComponent) {
        return DaggerDescriptionScreen_DescriptionComponent.builder()
                .detailComponent(parentComponent)
                .descriptionModule(new DescriptionModule())
                .build();
    }

    // region ==================== DI ====================

    @dagger.Module
    class DescriptionModule {
        @Provides
        @DaggerScope(DescriptionScreen.class)
        DescriptionPresenter provideDescriptionPresenter () {
            return new DescriptionPresenter(mProductRealm);
        }
    }

    @dagger.Component(dependencies = DetailScreen.DetailComponent.class, modules = DescriptionModule.class)
    @DaggerScope(DescriptionScreen.class)
    interface DescriptionComponent {
        void inject(DescriptionPresenter presenter);
        void inject(DescriptionView view);
    }

    // endregion
}