package ru.skillbranch.catalogapp.features.detail;

import android.os.Bundle;

import mortar.MortarScope;
import ru.skillbranch.catalogapp.data.storage.ProductRealm;
import ru.skillbranch.catalogapp.di.DaggerService;
import ru.skillbranch.catalogapp.mvp.AbstractPresenter;

class DetailPresenter extends AbstractPresenter<DetailView, DetailModel>
        implements IDetailPresenter {

    private ProductRealm mProduct;

    DetailPresenter(ProductRealm product) {
        mProduct = product;
    }

    @Override
    protected void onLoad(Bundle savedInstanceState) {
        super.onLoad(savedInstanceState);

        if (getView() != null) {
            getView().initView(mProduct);
        }
    }

    @Override
    public void dropView(DetailView view) {
        super.dropView(view);
    }

    // region ==================== AbstractPresenter ====================

    @Override
    protected void initDagger(MortarScope scope) {
        ((DetailScreen.DetailComponent) scope.getService(DaggerService.SERVICE_NAME)).inject(this);
    }

    @Override
    protected void initActionBar() {
        if (getView() != null) {
            mRootPresenter.newActionBarBuilder()
                    .setTitle(mProduct.getProductName())
                    .setBackArrow(true)
                    .setTab(getView().getViewPager())
                    .build();
        }
    }

    // endregion
}