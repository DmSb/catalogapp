package ru.skillbranch.catalogapp.features.account;

import ru.skillbranch.catalogapp.data.dto.AddressDto;

interface IAccountPresenter {
    void switchOrder(boolean isChecked);
    void switchPromo(boolean isChecked);

    void chooseCamera();
    void chooseGallery();

    void clickOnAddAddress();
    void removeAddress(String addressId);
    void editAddress(AddressDto address);
    void saveUserProfileInfo();

    void takePhoto();
}
