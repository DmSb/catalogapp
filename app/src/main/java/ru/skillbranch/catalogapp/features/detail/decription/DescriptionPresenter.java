package ru.skillbranch.catalogapp.features.detail.decription;

import android.os.Bundle;

import io.realm.Realm;
import io.realm.RealmChangeListener;
import mortar.MortarScope;
import ru.skillbranch.catalogapp.R;
import ru.skillbranch.catalogapp.data.storage.ProductRealm;
import ru.skillbranch.catalogapp.di.DaggerService;
import ru.skillbranch.catalogapp.features.detail.DetailModel;
import ru.skillbranch.catalogapp.mvp.AbstractPresenter;
import ru.skillbranch.catalogapp.utils.Lg;

class DescriptionPresenter extends AbstractPresenter<DescriptionView, DetailModel> {
    private final ProductRealm mProductRealm;
    private RealmChangeListener mListener;

    DescriptionPresenter(ProductRealm productRealm) {
        mProductRealm = productRealm;
    }

    @Override
    protected void onLoad(Bundle savedInstanceState) {
        super.onLoad(savedInstanceState);
        if (getView() != null) {
            getView().initView(mProductRealm);
            initFab();
        }
        mListener = element -> {
            if (getView() != null) {
                getView().initView(mProductRealm);
            }
        };
        mProductRealm.addChangeListener(mListener);
    }

    @Override
    public void dropView(DescriptionView view) {
        mProductRealm.removeChangeListener(mListener);
        super.dropView(view);
    }

    @Override
    protected void initDagger(MortarScope scope) {
        ((DescriptionScreen.DescriptionComponent) scope.getService(DaggerService.SERVICE_NAME)).inject(this);
    }

    @Override
    protected void initActionBar() {
        // empty
    }

    void minusOnClick() {
        if (mProductRealm.getCount() > 0) {
            Realm realm = Realm.getDefaultInstance();
            realm.executeTransaction(realm1 -> mProductRealm.deleteProduct());
            realm.close();

        }
    }

    void plusOnClick() {
        Realm realm = Realm.getDefaultInstance();
        realm.executeTransaction(realm1 -> mProductRealm.addProduct());
        realm.close();
    }

    void initFab() {
        Lg.d("initFab");
        mRootPresenter.newFabBuilder()
                .setFabVisible(true)
                .setImageResId(R.drawable.ic_favorite)
                .setClickListener(null)
                .build();
    }
}
