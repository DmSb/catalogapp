package ru.skillbranch.catalogapp.features.address;

import android.support.annotation.Nullable;

import dagger.Provides;
import flow.TreeKey;
import ru.skillbranch.catalogapp.R;
import ru.skillbranch.catalogapp.data.dto.AddressDto;
import ru.skillbranch.catalogapp.features.account.AccountScreen;
import ru.skillbranch.catalogapp.flow.AbstractScreen;
import ru.skillbranch.catalogapp.flow.Screen;

@Screen(R.layout.screen_address)
public class AddressScreen extends AbstractScreen<AccountScreen.AccountComponent> implements TreeKey {

    private AddressDto mAddress;

    public AddressScreen(@Nullable AddressDto address) {
        if (address != null) {
            mAddress = address;
        } else {
            mAddress = new AddressDto();
        }
    }

    @Override
    public boolean equals(Object o) {
        if (mAddress != null) {
            return o instanceof AddressScreen && mAddress.equals(((AddressScreen) o).mAddress);
        } else {
            return super.equals(o);
        }
    }

    @Override
    public int hashCode() {
        return mAddress != null ? mAddress.hashCode() : super.hashCode();
    }

    @Override
    public Object createScreenComponent(AccountScreen.AccountComponent parentComponent) {
        return DaggerAddressScreen_AddressComponent.builder()
                .accountComponent(parentComponent)
                .addressModule(new AddressModule())
                .build();
    }

    @Override
    public Object getParentKey() {
        return new AccountScreen();
    }

    public AddressDto getAddress() {
        return mAddress;
    }

    // region ==================== DI ====================

    @dagger.Module
    class AddressModule {
        @Provides
        @AddressScope
        AddressPresenter provideAddressPresenter () {
            return new AddressPresenter();
        }
    }

    @AddressScope
    @dagger.Component(dependencies = AccountScreen.AccountComponent.class, modules = AddressScreen.AddressModule.class)
    interface AddressComponent {
        void inject(AddressView view);
        void inject(AddressPresenter presenter);
    }

    // endregion
}
