package ru.skillbranch.catalogapp.features.auth;

public interface IAuthPresenter {

    void clickOnLogin();
    void clickOnFb();
    void clickOnVk();
    void clickOnTwitter();
    void clickOnShowCatalog();

    boolean checkUserAuth();
}
