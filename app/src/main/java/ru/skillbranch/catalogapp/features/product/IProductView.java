package ru.skillbranch.catalogapp.features.product;

import ru.skillbranch.catalogapp.data.dto.ProductDto;
import ru.skillbranch.catalogapp.data.dto.ProductLocalDto;
import ru.skillbranch.catalogapp.mvp.IView;

interface IProductView extends IView {
    void showProductView(ProductDto product);
    void updateProductCountView(ProductDto product);
    void updateFavorite(ProductDto product);
    ProductLocalDto getProductLocalInfo();
}