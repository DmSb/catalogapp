package ru.skillbranch.catalogapp.features.root;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.view.View;

import com.fernandocejas.frodo.annotation.RxLogSubscriber;

import javax.inject.Inject;

import mortar.Presenter;
import mortar.bundler.BundleService;
import ru.skillbranch.catalogapp.App;
import ru.skillbranch.catalogapp.data.dto.ActivityResultDto;
import ru.skillbranch.catalogapp.data.dto.PermissionResultDto;
import ru.skillbranch.catalogapp.data.dto.UserDto;
import ru.skillbranch.catalogapp.features.account.AccountModel;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subjects.PublishSubject;

import static android.content.pm.PackageManager.PERMISSION_GRANTED;

public class RootPresenter extends Presenter<IRootView> {

    @Inject
    AccountModel mAccountModel;

    private PublishSubject<ActivityResultDto> mActivityResultObs;
    private PublishSubject<PermissionResultDto> mPermissionResultObs;
    private Subscription mUserInfoSub;

    private static int DEFAULT_MODE = 0;
    private static int TAB_MODE = 1;

    public RootPresenter() {
        App.getRootActivityComponent().inject(this);
        mActivityResultObs = PublishSubject.create();
        mPermissionResultObs = PublishSubject.create();
    }

    @Override
    protected BundleService extractBundleService(IRootView view) {
        return BundleService.getBundleService((RootActivity) view);
    }

    /*
    @Override
    public void initView() {
        mAccountModel.getUserInfoObs()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new UserInfoSubscriber());

        if (getView() != null) {
            getView().setUseName(mAccountModel.getUser().getFullName());
            getView().loadUserAvatar(mAccountModel.getUser().getAvatar());
        }
    }


    public void setToolbarParam(boolean drawerMenuVisible, boolean productCardVisible, String title) {
        if (getView() != null) {
            mDrawerMenuVisible = drawerMenuVisible;
            getView().setDrawerMenuVisible(drawerMenuVisible);
            getView().setProductCardVisible(productCardVisible);
            getView().setToolbarTitle(title);
        }
    }

    public void setToolbarParam(boolean drawerMenuVisible, String title) {
        if (getView() != null) {
            mDrawerMenuVisible = drawerMenuVisible;
            getView().setDrawerMenuVisible(drawerMenuVisible);
            getView().setToolbarTitle(title);
        }
    }

    public boolean isDrawerMenuVisible() {
        return mDrawerMenuVisible;
    }*/

    public boolean checkPermissionsAndRequestIfNotGranted(@NonNull String[] permissions, int requestCode) {
        boolean allGranted = true;
        for (String permission : permissions) {
            int selfPermission = ContextCompat.checkSelfPermission((RootActivity) getView(), permission);
            if (selfPermission != PERMISSION_GRANTED) {
                allGranted = false;
                break;
            }
        }

        if (!allGranted) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                ((RootActivity) getView()).requestPermissions(permissions, requestCode);
                return false;
            }
        }

        return allGranted;
    }

    @Override
    protected void onLoad(Bundle savedInstanceState) {
        super.onLoad(savedInstanceState);
        if (getView() instanceof RootActivity) {
            mUserInfoSub = subscribeOnUserInfoObs();
        }
    }

    @Override
    public void dropView(IRootView view) {
        if (mUserInfoSub != null) {
            mUserInfoSub.unsubscribe();
        }
        super.dropView(view);
    }

    private Subscription subscribeOnUserInfoObs() {
        return mAccountModel.getUserInfoObs()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new UserInfoSubscriber());
    }

    // region ==================== ActivityResult ====================

    public PublishSubject<ActivityResultDto> getActivityResultObs() {
        return mActivityResultObs;
    }

    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        mActivityResultObs.onNext(new ActivityResultDto(requestCode, resultCode, intent));
    }

    // endregion

    // region ==================== PermissionsResult ====================

    public PublishSubject<PermissionResultDto> getPermissionResultObs() {
        return mPermissionResultObs;
    }

    public void onRequestPermissionResult(int requestCode, String[] permissions, int[] grantResults) {
        mPermissionResultObs.onNext(new PermissionResultDto(requestCode, permissions, grantResults));
        /*
        switch (requestCode) {
            case ConstantManager.REQUEST_PERMISSION_CAMERA:
                if (grantResults.length == 2
                        && grantResults[0] == PERMISSION_GRANTED
                        && grantResults[1] == PERMISSION_GRANTED) {
                    mPermissionsResultObs.onNext(REQUEST_PERMISSION_CAMERA);
                }
                break;
            case ConstantManager.REQUEST_PERMISSION_READ_EXTERNAL_STORAGE:
                if (grantResults.length == 1
                        && grantResults[0] == PERMISSION_GRANTED) {
                    mPermissionsResultObs.onNext(REQUEST_PERMISSION_READ_EXTERNAL_STORAGE);
                }
                break;
        }
        */
    }

    @Nullable
    public IRootView getRootView() {
        return getView();
    }

    // endregion

    // region ==================== Subscriber ====================

    @RxLogSubscriber
    private class UserInfoSubscriber extends Subscriber<UserDto> {

        @Override
        public void onCompleted() {

        }

        @Override
        public void onError(Throwable e) {
            if (getView() != null) {
                getView().showError(e);
            }
        }

        @Override
        public void onNext(UserDto user) {
            if (getView() != null) {
                getView().setUseName(user.getFullName());
                getView().loadUserAvatar(user.getAvatar());
            }
        }
    }

    // endregion

    public ActionBarBuilder newActionBarBuilder() {
        return this.new ActionBarBuilder();
    }

    public class ActionBarBuilder {
        private boolean isGoBack = false;
        private boolean isVisible = false;
        private boolean isProductCardVisible = false;
        private CharSequence title;
        private ViewPager pager;
        private int toolBarMode = DEFAULT_MODE;

        public ActionBarBuilder setTitle(CharSequence title) {
            this.title = title;
            return this;
        }

        public ActionBarBuilder setBackArrow(boolean enable) {
            this.isGoBack = enable;
            return this;
        }

        public ActionBarBuilder setVisible(boolean visible) {
            this.isVisible = visible;
            return this;
        }

        public ActionBarBuilder setTab(ViewPager pager) {
            this.pager = pager;
            this.toolBarMode = TAB_MODE;
            return this;
        }

        public ActionBarBuilder setProductCardVisible(boolean visible) {
            this.isProductCardVisible = visible;
            return this;
        }

        public void build() {
            if (getView() != null) {
                RootActivity activity = (RootActivity) getView();
                activity.setBarVisible(isVisible);
                activity.setBarTitle(title);
                activity.setBackArrow(isGoBack);
                activity.setProductCardVisible(isProductCardVisible);
                if (toolBarMode == TAB_MODE) {
                    activity.setTabLayout(pager);
                } else {
                    activity.removeTabLayout();
                }
            }
        }
    }

    public FabBuilder newFabBuilder() {
        return this.new FabBuilder();
    }

    public class FabBuilder {
        private boolean fabVisible = false;
        private int imageResId = 0;
        private View.OnClickListener clickListener = null;

        public FabBuilder setFabVisible(boolean visible) {
            this.fabVisible = visible;
            return this;
        }

        public FabBuilder setImageResId(int imageResId) {
            this.imageResId = imageResId;
            return this;
        }

        public FabBuilder setClickListener(View.OnClickListener clickListener) {
            this.clickListener = clickListener;
            return this;
        }

        public void build() {
            if (getView() != null) {
                RootActivity activity = (RootActivity) getView();
                activity.setFabVisible(fabVisible);
                activity.setImageResId(imageResId);
                activity.setFabListener(clickListener);
            }
        }
    }
}