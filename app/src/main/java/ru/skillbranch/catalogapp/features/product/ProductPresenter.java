package ru.skillbranch.catalogapp.features.product;

import android.os.Bundle;

import flow.Flow;
import io.realm.RealmChangeListener;
import mortar.MortarScope;
import ru.skillbranch.catalogapp.data.dto.ProductDto;
import ru.skillbranch.catalogapp.data.storage.ProductRealm;
import ru.skillbranch.catalogapp.data.storage.RealmTransactionExecutor;
import ru.skillbranch.catalogapp.di.DaggerService;
import ru.skillbranch.catalogapp.features.catalog.CatalogModel;
import ru.skillbranch.catalogapp.features.detail.DetailScreen;
import ru.skillbranch.catalogapp.mvp.AbstractPresenter;

class ProductPresenter extends AbstractPresenter<ProductView, CatalogModel>
        implements IProductPresenter {

    private ProductRealm mProduct;
    private RealmChangeListener mListener;

    ProductPresenter(ProductRealm product) {
        mProduct = product;
    }

    // region ==================== Life Cycle ====================

    @Override
    protected void onLoad(Bundle savedInstanceState) {
        super.onLoad(savedInstanceState);
        if (getView() != null && mProduct.isValid()) {
            getView().showProductView(new ProductDto(mProduct));

            mListener = element -> {
                if (getView() != null){
                    getView().showProductView(new ProductDto(mProduct));
                }
            };
            mProduct.addChangeListener(mListener);
        }
    }

    @Override
    protected void initDagger(MortarScope scope) {
        ((ProductScreen.ProductComponent) scope.getService(DaggerService.SERVICE_NAME)).inject(this);
    }

    @Override
    protected void initActionBar() {
        // empty
    }

    @Override
    public void dropView(ProductView view) {
        mProduct.removeChangeListener(mListener);
        super.dropView(view);
    }

    // endregion

    // region ==================== IProductPresenter ====================

    @Override
    public void clickOnPlus() {
        RealmTransactionExecutor.executeRealmTransaction(realm -> mProduct.addProduct());

        if (getView() != null) {
            getView().updateProductCountView(new ProductDto(mProduct));
        }
    }

    @Override
    public void clickOnMinus() {
        if (mProduct.getCount() > 0) {
            RealmTransactionExecutor.executeRealmTransaction(realm -> mProduct.deleteProduct());

            if (getView() != null) {
                getView().updateProductCountView(new ProductDto(mProduct));
            }
        }
    }

    @Override
    public void clickOnFavorite() {
        RealmTransactionExecutor.executeRealmTransaction(realm -> mProduct.changeFavorite());

        if (getView() != null) {
            getView().updateFavorite(new ProductDto(mProduct));
        }
    }

    @Override
    public void clickOnShowMore() {
        if (getView() != null) {
            Flow.get(getView()).set(new DetailScreen(mProduct));
        }
    }

    // endregion
}