package ru.skillbranch.catalogapp.features.detail.comments;

import dagger.Provides;
import ru.skillbranch.catalogapp.R;
import ru.skillbranch.catalogapp.data.storage.ProductRealm;
import ru.skillbranch.catalogapp.di.DaggerScope;
import ru.skillbranch.catalogapp.features.detail.DetailScreen;
import ru.skillbranch.catalogapp.flow.AbstractScreen;
import ru.skillbranch.catalogapp.flow.Screen;

@Screen(R.layout.screen_comment)
public class CommentScreen extends AbstractScreen<DetailScreen.DetailComponent> {
    private ProductRealm mProduct;

    public CommentScreen(ProductRealm product) {
        mProduct = product;
    }

    @Override
    public Object createScreenComponent(DetailScreen.DetailComponent parentComponent) {
        return DaggerCommentScreen_CommentComponent.builder()
                .detailComponent(parentComponent)
                .commentModule(new CommentModule())
                .build();
    }

    // region ==================== DI ====================

    @dagger.Module
    class CommentModule {
        @Provides
        @DaggerScope(CommentScreen.class)
        CommentPresenter provideCommentPresenter () {
            return new CommentPresenter(mProduct);
        }
    }

    @dagger.Component(dependencies = DetailScreen.DetailComponent.class, modules = CommentScreen.CommentModule.class)
    @DaggerScope(CommentScreen.class)
    interface CommentComponent {
        void inject(CommentPresenter presenter);
        void inject(CommentView view);
        void inject(CommentAdapter adapter);
    }

    // endregion
}