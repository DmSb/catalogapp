package ru.skillbranch.catalogapp.features.root;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.leakcanary.RefWatcher;
import com.squareup.picasso.Picasso;

import java.util.List;

import javax.inject.Inject;

import flow.Direction;
import flow.Flow;
import mortar.MortarScope;
import mortar.bundler.BundleServiceRunner;
import ru.skillbranch.catalogapp.App;
import ru.skillbranch.catalogapp.BuildConfig;
import ru.skillbranch.catalogapp.R;
import ru.skillbranch.catalogapp.databinding.ActivityRootBinding;
import ru.skillbranch.catalogapp.di.DaggerService;
import ru.skillbranch.catalogapp.di.modules.PicassoCacheModule;
import ru.skillbranch.catalogapp.features.account.AccountModel;
import ru.skillbranch.catalogapp.features.account.AccountScreen;
import ru.skillbranch.catalogapp.features.auth.AuthScreen;
import ru.skillbranch.catalogapp.features.catalog.CatalogScreen;
import ru.skillbranch.catalogapp.flow.TreeKeyDispatcher;
import ru.skillbranch.catalogapp.mortar.ScreenScoper;
import ru.skillbranch.catalogapp.mvp.IActionBarView;
import ru.skillbranch.catalogapp.mvp.IFabView;
import ru.skillbranch.catalogapp.mvp.IView;
import ru.skillbranch.catalogapp.mvp.MenuItemHolder;
import ru.skillbranch.catalogapp.ui.activities.BaseActivity;
import ru.skillbranch.catalogapp.utils.TransformRounded;

public class RootActivity extends BaseActivity
        implements IRootView, NavigationView.OnNavigationItemSelectedListener, IActionBarView, IFabView {
    private ActivityRootBinding mBinding;
    private ActionBarDrawerToggle mToggle;
    private ActionBar mActionBar;
    private List<MenuItemHolder> mActionBarMenuItems;

    @Inject
    RootPresenter mRootPresenter;
    @Inject
    Picasso mPicasso;

    // region ==================== Life Cycle ====================

    @Override
    protected void attachBaseContext(Context newBase) {
        newBase = Flow.configure(newBase, this)
                .defaultKey(new AuthScreen())
                .dispatcher(new TreeKeyDispatcher(this))
                .install();
        super.attachBaseContext(newBase);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_root);
        setTheme(R.style.AppTheme);
        BundleServiceRunner.getBundleServiceRunner(this).onCreate(savedInstanceState);
        createDaggerComponent();
        initToolBar();
    }

    @Override
    protected void onStart() {
        super.onStart();
        mRootPresenter.takeView(this);
    }

    @Override
    protected void onStop() {
        mBinding.fab.setOnClickListener(null);
        deAttachDrawer();
        mRootPresenter.dropView(this);
        super.onStop();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        BundleServiceRunner.getBundleServiceRunner(this).onSaveInstanceState(outState);
    }

    @Override
    protected void onDestroy() {
        if (isFinishing()) {
            ScreenScoper.destroyScreenScope(AuthScreen.class.getName());
        }
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        if (getCurrentScreen() != null && !getCurrentScreen().viewOnBackPressed() && !Flow.get(this).goBack()) {
            if (mBinding.drawerLayout.isDrawerVisible(GravityCompat.START)) {
                mBinding.drawerLayout.closeDrawer(GravityCompat.START);
            } else {
                super.onBackPressed();
            }
        }
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        if (mActionBarMenuItems != null && mActionBarMenuItems.isEmpty()) {
            for (MenuItemHolder menuItem : mActionBarMenuItems) {
                MenuItem item = menu.add(menuItem.getItemTitle());
                item.setShowAsActionFlags(MenuItem.SHOW_AS_ACTION_ALWAYS)
                        .setIcon(menuItem.getIconResId())
                        .setOnMenuItemClickListener(menuItem.getListener());
            }
        } else {
            menu.clear();
        }
        return super.onPrepareOptionsMenu(menu);
    }

    // endregion

    // region ==================== Init ====================

    @Override
    public Object getSystemService(@NonNull String name) {
        MortarScope rootActivityScope = MortarScope.findChild(getApplicationContext(),
                RootActivity.class.getName());
        return rootActivityScope.hasService(name) ? rootActivityScope.getService(name) : super.getSystemService(name);
    }

    private void createDaggerComponent() {
        RootComponent rootComponent = DaggerService.getDaggerComponent(this);
        rootComponent.inject(this);
    }

    private void initToolBar() {
        setSupportActionBar(mBinding.toolbar);
        mActionBar = getSupportActionBar();
        mToggle = new ActionBarDrawerToggle(this,
                mBinding.drawerLayout,
                mBinding.toolbar,
                R.string.open_drawer,
                R.string.close_drawer);
        mBinding.drawerLayout.addDrawerListener(mToggle);
        mBinding.navView.setNavigationItemSelectedListener(this);
        mToggle.syncState();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        mRootPresenter.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        mRootPresenter.onRequestPermissionResult(requestCode, permissions, grantResults);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.nav_account:
                showAccountScreen();
                break;
            case R.id.nav_catalog:
                showCatalogScreen();
                break;
            case R.id.nav_favorite:
                break;
            case R.id.nav_orders:
                break;
            case R.id.nav_notification:
                break;
        }
        mBinding.drawerLayout.closeDrawer(GravityCompat.START);
        return true;
    }

    private void deAttachDrawer() {
        mBinding.navView.setNavigationItemSelectedListener(null);
    }

    // endregion

    // region ==================== IRootView, IView ====================

    @Override
    public void showMessage(String message) {
        Snackbar.make(mBinding.coordinatorFrame, message, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void showError(Throwable e) {
        if (BuildConfig.DEBUG) {
            showMessage(e.getMessage());
            e.printStackTrace();
        } else {
            showMessage(getString(R.string.error_message));
        }
    }

    @Override
    public void showPermissionError() {
        Snackbar.make(mBinding.coordinatorFrame, getString(R.string.root_permission_error),
                Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void showLoad() {
        showProgress();
    }

    @Override
    public void hideLoad() {
        hideProgress();
    }

    @Override
    public void loadUserAvatar(String userAvatar) {
        ImageView imgAvatar = (ImageView) mBinding.navView.getHeaderView(0).findViewById(R.id.user_avatar);
        mPicasso.load(Uri.parse(userAvatar))
                .placeholder(R.drawable.ic_account_circle_135dp)
                .transform(new TransformRounded())
                .resize(60, 60)
                .centerCrop()
                .into(imgAvatar);
    }

    @Override
    public void setUseName(String userName) {
        TextView textView = (TextView) mBinding.navView.getHeaderView(0).findViewById(R.id.user_name);
        textView.setText(userName);
    }

    @Override
    public void showAuthScreen(int customState) {
        Flow.get(this).replaceHistory(new AuthScreen(customState), Direction.REPLACE);
    }

    @Override
    public void showCatalogScreen() {
        Flow.get(this).replaceHistory(new CatalogScreen(), Direction.REPLACE);
        MenuItem item = mBinding.navView.getMenu().findItem(R.id.nav_catalog);
        if (item != null) {
            item.setChecked(true);
        }
    }

    @Override
    public void showAccountScreen() {
        Flow.get(this).replaceHistory(new AccountScreen(), Direction.REPLACE);
    }

    @Override
    public void updateProductCounter(int productCount) {
        mBinding.productCountTxt.setText(String.valueOf(productCount));
    }

    @Nullable
    @Override
    public IView getCurrentScreen() {
        return (IView) mBinding.rootFrame.getChildAt(0);
    }

    // endregion

    // region ==================== IActionBarView ====================

    @Override
    public void setBarTitle(CharSequence title) {
        if (mActionBar != null) {
            if (title.length() == 0) {
                mActionBar.setDisplayShowTitleEnabled(false);
                mBinding.titleIcon.setVisibility(View.VISIBLE);
            } else {
                mBinding.titleIcon.setVisibility(View.GONE);
                mActionBar.setDisplayShowTitleEnabled(false);
                mActionBar.setTitle(title);
            }
        }
    }

    @Override
    public void setBarVisible(boolean visible) {

    }

    @Override
    public void setBackArrow(boolean enabled) {
        if (mToggle != null && mActionBar != null) {
            if (enabled) {
                mToggle.setDrawerIndicatorEnabled(false); // скрываем индикатор toggle
                mActionBar.setDisplayHomeAsUpEnabled(true); // устанавливаем индикатор тулбара
                if (mToggle.getToolbarNavigationClickListener() == null) {
                    mToggle.setToolbarNavigationClickListener(v -> onBackPressed()); // вешаем обработчик
                }
            } else {
                mActionBar.setDisplayHomeAsUpEnabled(false); // скрываем индиктора тулбара
                mToggle.setDrawerIndicatorEnabled(true); // активируем индиктора toggle
                mToggle.setToolbarNavigationClickListener(null); // зануляем обработчик на toggle
            }
            // если есть возможность вернуться назад (активна стрелка назад),
            // то блокируем раскрытие NavigationDrawer
            mBinding.drawerLayout.setDrawerLockMode(
                    enabled ? DrawerLayout.LOCK_MODE_LOCKED_CLOSED : DrawerLayout.LOCK_MODE_UNLOCKED);
            mToggle.syncState(); // синхронизируем состояние toggle с NavigationDrawer
        }
    }

    @Override
    public void setTabLayout(ViewPager pager) {
        View view = mBinding.appbarLayout.getChildAt(1);
        TabLayout tabView;
        if (view == null) {
            tabView = new TabLayout(this);
            tabView.setupWithViewPager(pager);
            mBinding.appbarLayout.addView(tabView);
            // регистрируем обработчик переключения по табам для ViewPager
            pager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabView));
        } else {
            tabView = (TabLayout) view;
            tabView.setupWithViewPager(pager);
            pager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabView));
        }
    }

    @Override
    public void removeTabLayout() {
        View tabView = mBinding.appbarLayout.getChildAt(1);
        // проверяем есть ли у AppBar дочерняя View, являющаяся TabLayout
        if (tabView != null && tabView instanceof TabLayout) {
            // удаляем ее
            mBinding.appbarLayout.removeView(tabView);
        }
    }

    @Override
    public void setProductCardVisible(boolean visible){
        if (visible) {
            mBinding.productCountWrap.setVisibility(View.VISIBLE);
        } else {
            mBinding.productCountWrap.setVisibility(View.GONE);
        }
    }

    @Override
    public void setFab(int fabResId) {
        if (fabResId > 0) {
            mBinding.fab.setVisibility(View.VISIBLE);
            mBinding.fab.setImageResource(fabResId);
        } else {
            mBinding.fab.setVisibility(View.GONE);
        }
    }

    // endregion

    // region ==================== IFabView ====================

    @Override
    public void setFabVisible(boolean visible) {
        if (visible) {
            mBinding.fab.setVisibility(View.VISIBLE);
        } else {
            mBinding.fab.setVisibility(View.GONE);
            if (mBinding.fab.hasOnClickListeners()) {
                mBinding.fab.setOnClickListener(null);
            }
        }
    }

    @Override
    public void setImageResId(int imageResId) {
        if (imageResId > 0) {
            mBinding.fab.setVisibility(View.GONE);
            mBinding.fab.setImageDrawable(getResources().getDrawable(imageResId));
            mBinding.fab.setVisibility(View.VISIBLE);
        } else {
            mBinding.fab.setImageDrawable(null);
        }
    }

    @Override
    public void setFabListener(View.OnClickListener listener) {
        mBinding.fab.setOnClickListener(listener);
    }

    // endregion

    // region ==================== DI ====================

    @RootScope
    @dagger.Component(dependencies = App.AppComponent.class,
            modules = {RootModule.class, PicassoCacheModule.class})
    public interface RootComponent {
        void inject(RootActivity activity);
        void inject(RootPresenter presenter);

        RootPresenter getRootPresenter();
        Picasso getPicasso();
        AccountModel getAccountModel();
        RefWatcher getRefWatcher();
    }

    // endregion
}