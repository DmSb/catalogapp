package ru.skillbranch.catalogapp.features.catalog;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import mortar.MortarScope;
import ru.skillbranch.catalogapp.R;
import ru.skillbranch.catalogapp.data.storage.ProductRealm;

class CatalogAdapter extends PagerAdapter {
    private List<ProductRealm> mProductList = new ArrayList<>();

    @Override
    public int getCount() {
        return mProductList.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        ProductRealm product = mProductList.get(position);
        Context productContext = CatalogScreen.Factory.createProductContext(product, container.getContext());
        LayoutInflater inflater = LayoutInflater.from(productContext);
        View newView = inflater.inflate(R.layout.screen_product, container, false);
        container.addView(newView);
        return newView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        MortarScope screenScope = MortarScope.getScope(((View) object).getContext());
        container.removeView((View) object);
        screenScope.destroy();
    }

    void addItem(ProductRealm product) {
        mProductList.add(product);
        notifyDataSetChanged();
    }

    ProductRealm getProductByPos(int position) {
        return mProductList.get(position);
    }
}