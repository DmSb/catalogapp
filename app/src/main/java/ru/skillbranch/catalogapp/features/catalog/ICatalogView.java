package ru.skillbranch.catalogapp.features.catalog;

import ru.skillbranch.catalogapp.mvp.IView;

interface ICatalogView extends IView {

    void showCatalogView();
    void updatePagerIndicator();
}
