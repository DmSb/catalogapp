package ru.skillbranch.catalogapp.features.root;

import dagger.Module;
import dagger.Provides;
import ru.skillbranch.catalogapp.features.account.AccountModel;

@Module
public class RootModule {
    @Provides
    @RootScope
    RootPresenter provideRootPresenter () {
        return new RootPresenter();
    }
    
    @Provides
    @RootScope
    AccountModel provideAccountModel () {
        return new AccountModel();
    }
}