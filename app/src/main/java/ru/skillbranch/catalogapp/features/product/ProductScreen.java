package ru.skillbranch.catalogapp.features.product;

import dagger.Provides;
import ru.skillbranch.catalogapp.R;
import ru.skillbranch.catalogapp.data.storage.ProductRealm;
import ru.skillbranch.catalogapp.di.DaggerScope;
import ru.skillbranch.catalogapp.features.catalog.CatalogScreen;
import ru.skillbranch.catalogapp.flow.AbstractScreen;
import ru.skillbranch.catalogapp.flow.Screen;

@Screen(R.layout.screen_product)
public class ProductScreen extends AbstractScreen<CatalogScreen.CatalogComponent>{

    private ProductRealm mProduct;

    public ProductScreen(ProductRealm product) {
        mProduct = product;
    }

    @Override
    public boolean equals(Object o) {
        return o instanceof ProductScreen && mProduct.equals(((ProductScreen) o).mProduct);
    }

    @Override
    public int hashCode() {
        return mProduct != null ? mProduct.hashCode() : 0;
    }

    // region ==================== AbstractScreen ====================

    @Override
    public Object createScreenComponent(CatalogScreen.CatalogComponent parentComponent) {
        return DaggerProductScreen_ProductComponent.builder()
                .catalogComponent(parentComponent)
                .productModule(new ProductModule())
                .build();
    }

    // endregion

    // region ==================== DI ====================

    @dagger.Module
    class ProductModule {
        @Provides
        @DaggerScope(ProductScreen.class)
        ProductPresenter provideProductPresenter () {
            return new ProductPresenter(mProduct);
        }
    }

    @dagger.Component(dependencies = CatalogScreen.CatalogComponent.class,
            modules = ProductScreen.ProductModule.class)
    @DaggerScope(ProductScreen.class)
    interface ProductComponent {
        void inject(ProductPresenter presenter);
        void inject(ProductView view);
    }

    // endregion
}