package ru.skillbranch.catalogapp.features.detail;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;

import ru.skillbranch.catalogapp.data.storage.ProductRealm;
import ru.skillbranch.catalogapp.databinding.ScreenDetailBinding;
import ru.skillbranch.catalogapp.di.DaggerService;
import ru.skillbranch.catalogapp.mvp.AbstractView;
import ru.skillbranch.catalogapp.mvp.IInitFabView;

public class DetailView extends AbstractView<DetailPresenter> implements IDetailView {

    private ScreenDetailBinding mBinding;
    private ViewPager.OnPageChangeListener mListener;

    public DetailView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    // region ==================== AbstractView ====================

    @Override
    protected void initDagger(Context context) {
        DaggerService.<DetailScreen.DetailComponent>getDaggerComponent(context).inject(this);
    }

    @Override
    protected void initBinding() {
        mBinding = DataBindingUtil.bind(this);
    }

    @Override
    protected void attachListeners() {

        mListener = new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                ((IInitFabView) mBinding.productInfoPager.getChildAt(position)).initFab();
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        };

        mBinding.productInfoPager.addOnPageChangeListener(mListener);
    }

    @Override
    protected void detachListeners() {
        mBinding.productInfoPager.removeOnPageChangeListener(mListener);
    }

    // endregion

    // region ==================== IDetailView, IView ====================

    @Override
    public boolean viewOnBackPressed() {
        return false;
    }

    @Override
    public void initView(ProductRealm product) {
        if (!isInEditMode()){
            DetailAdapter adapter = new DetailAdapter(product);
            mBinding.productInfoPager.setAdapter(adapter);
        }
    }

    @Override
    public ViewPager getViewPager() {
        return mBinding.productInfoPager;
    }

    // endregion
}