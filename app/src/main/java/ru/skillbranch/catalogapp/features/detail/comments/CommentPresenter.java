package ru.skillbranch.catalogapp.features.detail.comments;

import android.os.Bundle;

import java.util.List;

import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmList;
import mortar.MortarScope;
import ru.skillbranch.catalogapp.R;
import ru.skillbranch.catalogapp.data.dto.CommentDto;
import ru.skillbranch.catalogapp.data.storage.CommentRealm;
import ru.skillbranch.catalogapp.data.storage.ProductRealm;
import ru.skillbranch.catalogapp.di.DaggerService;
import ru.skillbranch.catalogapp.features.detail.DetailModel;
import ru.skillbranch.catalogapp.mvp.AbstractPresenter;
import ru.skillbranch.catalogapp.utils.Lg;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;

public class CommentPresenter extends AbstractPresenter<CommentView, DetailModel> {
    private final ProductRealm mProduct;

    public CommentPresenter(ProductRealm product) {
        mProduct = product;
    }

    @Override
    protected void initDagger(MortarScope scope) {
        ((CommentScreen.CommentComponent) scope.getService(DaggerService.SERVICE_NAME)).inject(this);
    }

    @Override
    protected void onLoad(Bundle savedInstanceState) {
        super.onLoad(savedInstanceState);

        mProduct.addChangeListener((RealmChangeListener<ProductRealm>) element -> {
            updateProductList(element);
        });

        RealmList<CommentRealm> comments = mProduct.getComments();
        Observable<CommentDto> commentObs = Observable.from(comments)
                .subscribeOn(AndroidSchedulers.mainThread())
                .map(CommentDto::new);

        if (getView() != null) {
            mCompSubs.add(subscribe(commentObs, new ViewSubscriber<CommentDto>() {
                @Override
                public void onNext(CommentDto commentDto) {
                    getView().getAdapter().addComments(commentDto);
                }
            }));
            getView().initView();
        }
    }

    private void updateProductList(ProductRealm element) {
        Lg.d("updateProductList");
        Observable<List<CommentDto>> obs = Observable.from(element.getComments())
                .subscribeOn(AndroidSchedulers.mainThread())
                .map(CommentDto::new)
                .toList();

        mCompSubs.add(subscribe(obs, new ViewSubscriber<List<CommentDto>>() {
            @Override
            public void onNext(List<CommentDto> commentDtos) {
                getView().getAdapter().reloadAdapter(commentDtos);
            }
        }));
    }

    @Override
    public void dropView(CommentView view) {
        mProduct.removeChangeListeners();
        super.dropView(view);
    }

    @Override
    protected void initActionBar() {
        // empty
    }

    public void addComment(CommentRealm commentRealm) {
        Realm realm = Realm.getDefaultInstance();
        realm.executeTransaction(realm1 -> mProduct.getComments().add(commentRealm));
        realm.close();
    }

    public void initFab() {
        Lg.d("initFab");
        mRootPresenter.newFabBuilder()
                .setFabVisible(true)
                .setImageResId(R.drawable.ic_add)
                .setClickListener(v -> {
                    if (getView() != null) {
                        getView().showAddCommentDialog();
                    }
                })
                .build();
    }

    public String getUserName() {
        String userName = mModel.getUserName();
        if (userName == null || userName.isEmpty()) {
            userName = "No Name";
        }
        return userName;
    }

    public String getAvatar() {
        return mModel.getAvatar();
    }
}