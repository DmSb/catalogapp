package ru.skillbranch.catalogapp.features.detail.decription;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.util.AttributeSet;

import ru.skillbranch.catalogapp.data.dto.ProductDto;
import ru.skillbranch.catalogapp.data.storage.ProductRealm;
import ru.skillbranch.catalogapp.databinding.ScreenDescriptionBinding;
import ru.skillbranch.catalogapp.di.DaggerService;
import ru.skillbranch.catalogapp.mvp.AbstractView;
import ru.skillbranch.catalogapp.mvp.IInitFabView;

public class DescriptionView extends AbstractView<DescriptionPresenter> implements IInitFabView {

    private ScreenDescriptionBinding mBinding;
    private ProductRealm mProduct;

    // region ==================== Life cycle ====================

    public DescriptionView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void initDagger(Context context) {
        DaggerService.<DescriptionScreen.DescriptionComponent>getDaggerComponent(context).inject(this);
    }

    @Override
    protected void initBinding() {
        mBinding = DataBindingUtil.bind(this);
    }

    // endregion

    public void initView(ProductRealm product) {
        mProduct = product;
        setProduct();
    }

    protected void attachListeners() {
        mBinding.minusBtn.setOnClickListener(v -> {
            mPresenter.minusOnClick();
            setProduct();
        });

        mBinding.plusBtn.setOnClickListener(v -> {
            mPresenter.plusOnClick();
            setProduct();
        });
    }

    protected void detachListeners() {
        mBinding.minusBtn.setOnClickListener(null);
        mBinding.plusBtn.setOnClickListener(null);
    }

    public void setProduct() {
        ProductDto productDto = new ProductDto(mProduct);
        if (productDto.getCount() > 0) {
            productDto.setPrice(productDto.getCount() * productDto.getPrice());
        } else {
            productDto.setPrice(productDto.getPrice());
        }
        mBinding.setProduct(productDto);
    }

    @Override
    public boolean viewOnBackPressed() {
        return false;
    }

    @Override
    public void initFab() {
        mPresenter.initFab();
    }
}
