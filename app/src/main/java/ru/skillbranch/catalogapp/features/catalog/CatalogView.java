package ru.skillbranch.catalogapp.features.catalog;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.util.AttributeSet;
import android.view.View;

import ru.skillbranch.catalogapp.R;
import ru.skillbranch.catalogapp.databinding.ScreenCatalogBinding;
import ru.skillbranch.catalogapp.di.DaggerService;
import ru.skillbranch.catalogapp.mvp.AbstractView;

public class CatalogView
        extends AbstractView<CatalogPresenter>
        implements ICatalogView, View.OnClickListener {

    private ScreenCatalogBinding mBinding;
    private CatalogAdapter mAdapter = new CatalogAdapter();

    public CatalogView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    // region ==================== AbstractView ====================

    @Override
    protected void initDagger(Context context) {
        DaggerService.<CatalogScreen.CatalogComponent>getDaggerComponent(context).inject(this);
    }

    @Override
    protected void initBinding() {
        mBinding = DataBindingUtil.bind(this);
    }

    @Override
    protected void attachListeners() {
        mBinding.addToCardBtn.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.add_to_card_btn:
                mPresenter.clickOnByBtn(mBinding.productPager.getCurrentItem());
        }
    }

    @Override
    protected void detachListeners() {
        mBinding.addToCardBtn.setOnClickListener(null);
    }

    // endregion

    // region ==================== ICatalogView ====================

    @Override
    public void showCatalogView() {
        mBinding.productPager.setAdapter(mAdapter);
        mBinding.pagerIndicator.setViewPager(mBinding.productPager);
    }

    @Override
    public void updatePagerIndicator() {
        mBinding.pagerIndicator.getDataSetObserver().onChanged();
    }

    @Override
    public boolean viewOnBackPressed() {
        return false;
    }

    public CatalogAdapter getAdapter() {
        return mAdapter;
    }

    public int getCurrentPagerPosition() {
        return mBinding.productPager.getCurrentItem();
    }

    // endregion
}