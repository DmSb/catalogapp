package ru.skillbranch.catalogapp.features.product;

interface IProductPresenter {

    void clickOnPlus();
    void clickOnMinus();
    void clickOnFavorite();
    void clickOnShowMore();
}
