package ru.skillbranch.catalogapp.di.components;


import com.squareup.picasso.Picasso;

import dagger.Component;
import ru.skillbranch.catalogapp.App;
import ru.skillbranch.catalogapp.di.modules.PicassoCacheModule;
import ru.skillbranch.catalogapp.features.root.RootScope;

@Component(dependencies = App.AppComponent.class, modules = PicassoCacheModule.class)
@RootScope
public interface PicassoComponent {
    Picasso getPicasso();
}
