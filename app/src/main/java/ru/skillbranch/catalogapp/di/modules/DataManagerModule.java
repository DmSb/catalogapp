package ru.skillbranch.catalogapp.di.modules;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import ru.skillbranch.catalogapp.data.managers.DataManager;

@Module
public class DataManagerModule {
    @Provides
    @Singleton
    DataManager provideDataManager () {
        return DataManager.getInstance();
    }
}
