package ru.skillbranch.catalogapp.di.components;

import javax.inject.Singleton;

import dagger.Component;
import ru.skillbranch.catalogapp.di.modules.DataManagerModule;
import ru.skillbranch.catalogapp.mvp.AbstractModel;

@Singleton
@Component(modules = DataManagerModule.class)
public interface DataManagerComponent {
    void inject(AbstractModel abstractModel);
}
