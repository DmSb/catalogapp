package ru.skillbranch.catalogapp.di.modules;

import android.content.Context;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import ru.skillbranch.catalogapp.data.managers.PreferencesManager;

@Module
public class LocalModule extends FlavorLocalModule {

    @Provides
    @Singleton
    PreferencesManager providePreferencesManager (Context context) {
        return new PreferencesManager(context);
    }
}