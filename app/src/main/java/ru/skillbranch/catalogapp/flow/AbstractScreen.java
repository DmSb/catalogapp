package ru.skillbranch.catalogapp.flow;

import flow.ClassKey;
import ru.skillbranch.catalogapp.mortar.ScreenScoper;

public abstract class AbstractScreen<T> extends ClassKey {

    public String getScopeName() {
        return getClass().getName();
    }

    public abstract Object createScreenComponent(T parentComponent);

    void unregisterScope() {
        ScreenScoper.destroyScreenScope(getScopeName());
    }

    public int getLayoutResId() {
        int layout;

        Screen screen;
        screen = this.getClass().getAnnotation(Screen.class);
        if (screen == null) {
            throw new IllegalStateException("Блин, опять забыл анатацию @Screen для " + this.getScopeName());
        } else {
            layout = screen.value();
        }

        return layout;
    }
}
